﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene001Sound : MonoBehaviour
{
    public AudioClip playerSoundScene002_001;
    public AudioClip playerSoundScene002_002;
    public AudioClip bossSoundScene001;
    private AudioSource audioSourceBossSoundScene001;

    /*
    public GameObject screen;

    public float MailTime = 20.0f;
    */

    //プレイヤーが話し始める時間
    public float playerSoundScene002_001StartTime = 3.0f;
    public float playerSoundScene002_002StartTime = 10.0f;

    //シーンがスタートしてサンタが話始めるまでの時間
    public float bossSoundScene001StartTime = 5.0f;

    //シーンがスタートしてシーン２に切り替わるまでの時間
    public float scene002ChangeStartTime = 30.0f;

    // Start is called before the first frame update
    void Start()
    {
        audioSourceBossSoundScene001 = gameObject.GetComponent<AudioSource>();
        Invoke("StartBossSoundScene001", bossSoundScene001StartTime);
        Invoke("StartScene002Change", scene002ChangeStartTime);

        //Invoke("GetMail", MailTime);
        Invoke("StartPlayerSoundScene002_001", playerSoundScene002_001StartTime);
        Invoke("StartPlayerSoundScene002_002", playerSoundScene002_002StartTime);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    /*
    void GetMail()
    {
        Instantiate(screen, new Vector3(0.396, 1.23, -3.394));
    }

    private void Instantiate(GameObject screen, Vector3 vector3)
    {
        throw new NotImplementedException();
    }
    */

    //プレイヤーの声再生
    void StartPlayerSoundScene002_001()
    {
        //audiosouceの名称ボスのまま利用、時間なく
        audioSourceBossSoundScene001.PlayOneShot(playerSoundScene002_001);
    }
    void StartPlayerSoundScene002_002()
    {
        audioSourceBossSoundScene001.PlayOneShot(playerSoundScene002_002);
    }

    void StartBossSoundScene001()
    {
        audioSourceBossSoundScene001.PlayOneShot(bossSoundScene001);
    }

    void StartScene002Change()
    {
        SceneManager.LoadScene("Scene003");
    }
}
