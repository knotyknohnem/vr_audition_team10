﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoisedEmotions : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<TextMesh>().text = "この場面であなたはこのような感情を感じていますね\nこれらの感情はあなたの行動にどんな影響を与えていますか？\n\n";

        // 怒りの感情の表示
        if (StateManager.nageyaridaFlag)
        {
            this.GetComponent<TextMesh>().text += "「なげやりだ」\n";
            this.GetComponent<TextMesh>().color = new Color(1, 0, 0, 1);
        }
        if (StateManager.suneteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「すねている」\n";
        }
        if (StateManager.jiboujikininatteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「自暴自棄になっている」\n";
        }
        if (StateManager.kattoshiteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「カッとしている」\n";
        }
        if (StateManager.shitsuboushiteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「失望している」\n";
        }
        if (StateManager.fuyukaidearuFlag)
        {
            this.GetComponent<TextMesh>().text += "「不愉快である」\n";
        }
        if (StateManager.okotteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「怒っている」\n";
        }
        if (StateManager.kireruFlag)
        {
            this.GetComponent<TextMesh>().text += "「キレる」\n";
        }
        if (StateManager.nikuiFlag)
        {
            this.GetComponent<TextMesh>().text += "「憎い」\n";
        }
        if (StateManager.irairashitaFlag)
        {
            this.GetComponent<TextMesh>().text += "「いらいらした」\n";
        }
        if (StateManager.wazurawashiiFlag)
        {
            this.GetComponent<TextMesh>().text += "「わずらわしい」\n";
        }
        if (StateManager.haratadashiiFlag)
        {
            this.GetComponent<TextMesh>().text += "「腹立たしい」\n";
        }

        // 恐れの感情の表示
        if (StateManager.roubaishiteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「狼狽している」\n";
        }
        if (StateManager.douyoushiteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「動揺している」\n";
        }
        if (StateManager.ushirometaiFlag)
        {
            this.GetComponent<TextMesh>().text += "「うしろめたい」\n";
        }
        if (StateManager.menkuratteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「面食らっている」\n";
        }
        if (StateManager.bikkurishiteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「びっくりしている」\n";
        }
        if (StateManager.tohounikureteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「途方に暮れている」\n";
        }
        if (StateManager.kanashiiFlag)
        {
            this.GetComponent<TextMesh>().text += "「悲しい」\n";
        }
        if (StateManager.obieteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「おびえている」\n";
        }
        if (StateManager.shinpaishiteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「心配している」\n";
        }
        if (StateManager.kowaiFlag)
        {
            this.GetComponent<TextMesh>().text += "「怖い」\n";
        }
        if (StateManager.touwakushiteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「当惑している」\n";
        }
        if (StateManager.ochitukanaiFlag)
        {
            this.GetComponent<TextMesh>().text += "「落ち着かない」\n";
        }

        // 悲しみの感情の表示
        if (StateManager.mikagirareteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「見限られている」\n";
        }
        if (StateManager.wabishiiFlag)
        {
            this.GetComponent<TextMesh>().text += "「わびしい」\n";
        }
        if (StateManager.rakutanshiteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「落胆している」\n";
        }
        if (StateManager.kurushiiFlag)
        {
            this.GetComponent<TextMesh>().text += "「苦しい」\n";
        }
        if (StateManager.yarusenaiFlag)
        {
            this.GetComponent<TextMesh>().text += "「やるせない」\n";
        }
        if (StateManager.turaiFlag)
        {
            this.GetComponent<TextMesh>().text += "「つらい」\n";
        }
        if (StateManager.hituunaFlag)
        {
            this.GetComponent<TextMesh>().text += "「悲痛な」\n";
        }
        if (StateManager.kodokunaFlag)
        {
            this.GetComponent<TextMesh>().text += "「孤独な」\n";
        }
        if (StateManager.mijimenaFlag)
        {
            this.GetComponent<TextMesh>().text += "「みじめな」\n";
        }
        if (StateManager.uchihishigareteiruFlag)
        {
            this.GetComponent<TextMesh>().text += "「打ちひしがれている」\n";
        }
        if (StateManager.sabishiiFlag)
        {
            this.GetComponent<TextMesh>().text += "「さびしい」\n";
        }
        if (StateManager.munashiiFlag)
        {
            this.GetComponent<TextMesh>().text += "「むなしい」\n";
        }

        // 驚きの感情の表示
        if (StateManager.bouzenFlag)
        {
            this.GetComponent<TextMesh>().text += "「呆然」\n";
        }
        if (StateManager.houshinFlag)
        {
            this.GetComponent<TextMesh>().text += "「放心」\n";
        }
        if (StateManager.odorokiFlag)
        {
            this.GetComponent<TextMesh>().text += "「驚き」\n";
        }
        if (StateManager.gyuoutenFlag)
        {
            this.GetComponent<TextMesh>().text += "「仰天」\n";
        }
        if (StateManager.kyoutanFlag)
        {
            this.GetComponent<TextMesh>().text += "「驚嘆」\n";
        }
        if (StateManager.igaiFlag)
        {
            this.GetComponent<TextMesh>().text += "「意外」\n";
        }
        if (StateManager.bikkurisuruFlag)
        {
            this.GetComponent<TextMesh>().text += "「びっくりする」\n";
        }
        if (StateManager.shougekiFlag)
        {
            this.GetComponent<TextMesh>().text += "「衝撃」\n";
        }
        if (StateManager.touwakusuruFlag)
        {
            this.GetComponent<TextMesh>().text += "「当惑する」\n";
        }
        if (StateManager.fushinFlag)
        {
            this.GetComponent<TextMesh>().text += "「不信」\n";
        }
        if (StateManager.kyoutendouchiFlag)
        {
            this.GetComponent<TextMesh>().text += "「驚天動地」\n";
        }
        if (StateManager.akkenitorareruFlag)
        {
            this.GetComponent<TextMesh>().text += "「あっけにとられる」\n";
        }

        // 幸せの感情の表示
        if (StateManager.heionnaFlag)
        {
            this.GetComponent<TextMesh>().text += "「平穏な」\n";
        }
        if (StateManager.siawasenayorokobiFlag)
        {
            this.GetComponent<TextMesh>().text += "「幸せな喜び」\n";
        }
        if (StateManager.uchoutennaFlag)
        {
            this.GetComponent<TextMesh>().text += "「有頂天な」\n";
        }
        if (StateManager.kankiFlag)
        {
            this.GetComponent<TextMesh>().text += "「歓喜」\n";
        }
        if (StateManager.ochitukuFlag)
        {
            this.GetComponent<TextMesh>().text += "「落ち着く」\n";
        }
        if (StateManager.koufukukanFlag)
        {
            this.GetComponent<TextMesh>().text += "「幸福感」\n";
        }
        if (StateManager.manzokuFlag)
        {
            this.GetComponent<TextMesh>().text += "「満足」\n";
        }
        if (StateManager.uresiiFlag)
        {
            this.GetComponent<TextMesh>().text += "「嬉しい」\n";
        }
        if (StateManager.ooyorokobiFlag)
        {
            this.GetComponent<TextMesh>().text += "「大喜び」\n";
        }
        if (StateManager.tanosisounaFlag)
        {
            this.GetComponent<TextMesh>().text += "「楽しそうな」\n";
        }
        if (StateManager.sekkyokutekinaFlag)
        {
            this.GetComponent<TextMesh>().text += "「積極的な」\n";
        }

        // 嫌悪の感情の表示
        if (StateManager.taikutunaFlag)
        {
            this.GetComponent<TextMesh>().text += "「退屈」\n";
        }
        if (StateManager.unzariFlag)
        {
            this.GetComponent<TextMesh>().text += "「うんざり」\n";
        }
        if (StateManager.tsuyoikenoFlag)
        {
            this.GetComponent<TextMesh>().text += "「強い嫌悪」\n";
        }
        if (StateManager.batousuruFlag)
        {
            this.GetComponent<TextMesh>().text += "「罵倒する」\n";
        }
        if (StateManager.hidokukirauFlag)
        {
            this.GetComponent<TextMesh>().text += "「ひどく嫌う」\n";
        }
        if (StateManager.iyakeFlag)
        {
            this.GetComponent<TextMesh>().text += "「嫌気」\n";
        }
        if (StateManager.doutokuseinonaiFlag)
        {
            this.GetComponent<TextMesh>().text += "「道徳性のない」\n";
        }
        if (StateManager.zottosuruFlag)
        {
            this.GetComponent<TextMesh>().text += "「ぞっとする」\n";
        }
        if (StateManager.uzaiFlag)
        {
            this.GetComponent<TextMesh>().text += "「うざい」\n";
        }
        if (StateManager.mukatsukuFlag)
        {
            this.GetComponent<TextMesh>().text += "「むかつく」\n";
        }
        if (StateManager.zouoFlag)
        {
            this.GetComponent<TextMesh>().text += "「憎悪」\n";
        }
        if (StateManager.fukushuuFlag)
        {
            this.GetComponent<TextMesh>().text += "「復讐」\n";
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
