﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    // 怒りの感情の変数
    public static bool nageyaridaFlag = false;
    public static bool suneteiruFlag = false;
    public static bool jiboujikininatteiruFlag = false;
    public static bool kattoshiteiruFlag = false;
    public static bool shitsuboushiteiruFlag = false;
    public static bool fuyukaidearuFlag = false;
    public static bool okotteiruFlag = false;
    public static bool kireruFlag = false;
    public static bool nikuiFlag = false;
    public static bool irairashitaFlag = false;
    public static bool wazurawashiiFlag = false;
    public static bool haratadashiiFlag = false;
    public static bool back01Flag = false;
    public static bool decide01Flag = false;

    // 恐れの感情の変数
    public static bool roubaishiteiruFlag = false;
    public static bool douyoushiteiruFlag = false;
    public static bool ushirometaiFlag = false;
    public static bool menkuratteiruFlag = false;
    public static bool bikkurishiteiruFlag = false;
    public static bool tohounikureteiruFlag = false;
    public static bool kanashiiFlag = false;
    public static bool obieteiruFlag = false;
    public static bool shinpaishiteiruFlag = false;
    public static bool kowaiFlag = false;
    public static bool touwakushiteiruFlag = false;
    public static bool ochitukanaiFlag = false;
    public static bool back02Flag = false;
    public static bool decide02Flag = false;

    // 悲しみの感情の変数
    public static bool mikagirareteiruFlag = false;
    public static bool wabishiiFlag = false;
    public static bool rakutanshiteiruFlag = false;
    public static bool kurushiiFlag = false;
    public static bool yarusenaiFlag = false;
    public static bool turaiFlag = false;
    public static bool hituunaFlag = false;
    public static bool kodokunaFlag = false;
    public static bool mijimenaFlag = false;
    public static bool uchihishigareteiruFlag = false;
    public static bool sabishiiFlag = false;
    public static bool munashiiFlag = false;
    public static bool back03Flag = false;
    public static bool decide03Flag = false;

    // 驚きの感情の変数
    public static bool bouzenFlag = false;
    public static bool houshinFlag = false;
    public static bool odorokiFlag = false;
    public static bool gyuoutenFlag = false;
    public static bool kyoutanFlag = false;
    public static bool igaiFlag = false;
    public static bool bikkurisuruFlag = false;
    public static bool shougekiFlag = false;
    public static bool touwakusuruFlag = false;
    public static bool fushinFlag = false;
    public static bool kyoutendouchiFlag = false;
    public static bool akkenitorareruFlag = false;
    public static bool back04Flag = false;
    public static bool decide04Flag = false;

    // 幸せの感情の変数
    public static bool heionnaFlag = false;
    public static bool siawasenayorokobiFlag = false;
    public static bool uchoutennaFlag = false;
    public static bool kankiFlag = false;
    public static bool ochitukuFlag = false;
    public static bool koufukukanFlag = false;
    public static bool manzokuFlag = false;
    public static bool uresiiFlag = false;
    public static bool ooyorokobiFlag = false;
    public static bool tanosisounaFlag = false;
    public static bool sekkyokutekinaFlag = false;
    public static bool back05Flag = false;
    public static bool decide05Flag = false;

    // 嫌悪の感情の変数
    public static bool taikutunaFlag = false;
    public static bool unzariFlag = false;
    public static bool tsuyoikenoFlag = false;
    public static bool batousuruFlag = false;
    public static bool hidokukirauFlag = false;
    public static bool iyakeFlag = false;
    public static bool doutokuseinonaiFlag = false;
    public static bool zottosuruFlag = false;
    public static bool uzaiFlag = false;
    public static bool mukatsukuFlag = false;
    public static bool zouoFlag = false;
    public static bool fukushuuFlag = false;
    public static bool back06Flag = false;
    public static bool decide06Flag = false;

    // 怒りの感情のsetter／getter
    public bool NageyaridaFlag
    {
        get{ return nageyaridaFlag; }  //取得用
        set{ nageyaridaFlag = value; } //値入力用
    }

    public bool SuneteiruFlag
    {
        get { return suneteiruFlag; }  //取得用
        set { suneteiruFlag = value; } //値入力用
    }

    public bool JiboujikininatteiruFlag
    {
        get { return jiboujikininatteiruFlag; }  //取得用
        set { jiboujikininatteiruFlag = value; } //値入力用
    }

    public bool KattoshiteiruFlag
    {
        get { return kattoshiteiruFlag; }  //取得用
        set { kattoshiteiruFlag = value; } //値入力用
    }

    public bool ShitsuboushiteiruFlag
    {
        get { return shitsuboushiteiruFlag; }  //取得用
        set { shitsuboushiteiruFlag = value; } //値入力用
    }

    public bool FuyukaidearuFlag
    {
        get { return fuyukaidearuFlag; }  //取得用
        set { fuyukaidearuFlag = value; } //値入力用
    }

    public bool OkotteiruFlag
    {
        get { return okotteiruFlag; }  //取得用
        set { okotteiruFlag = value; } //値入力用
    }

    public bool KireruFlag
    {
        get { return kireruFlag; }  //取得用
        set { kireruFlag = value; } //値入力用
    }

    public bool NikuiFlag
    {
        get { return nikuiFlag; }  //取得用
        set { nikuiFlag = value; } //値入力用
    }

    public bool IrairashitaFlag
    {
        get { return irairashitaFlag; }  //取得用
        set { irairashitaFlag = value; } //値入力用
    }

    public bool WazurawashiiFlag
    {
        get { return wazurawashiiFlag; }  //取得用
        set { wazurawashiiFlag = value; } //値入力用
    }

    public bool HaratadashiiFlag
    {
        get { return haratadashiiFlag; }  //取得用
        set { haratadashiiFlag = value; } //値入力用
    }

    // 恐れの感情のsetter／getter
    public bool RoubaishiteiruFlag
    {
        get { return roubaishiteiruFlag; }  //取得用
        set { roubaishiteiruFlag = value; } //値入力用
    }

    public bool DouyoushiteiruFlag
    {
        get { return douyoushiteiruFlag; }  //取得用
        set { douyoushiteiruFlag = value; } //値入力用
    }

    public bool UshirometaiFlag
    {
        get { return ushirometaiFlag; }  //取得用
        set { ushirometaiFlag = value; } //値入力用
    }

    public bool MenkuratteiruFlag
    {
        get { return menkuratteiruFlag; }  //取得用
        set { menkuratteiruFlag = value; } //値入力用
    }

    public bool BikkurishiteiruFlag
    {
        get { return bikkurishiteiruFlag; }  //取得用
        set { bikkurishiteiruFlag = value; } //値入力用
    }

    public bool TohounikureteiruFlag
    {
        get { return tohounikureteiruFlag; }  //取得用
        set { tohounikureteiruFlag = value; } //値入力用
    }

    public bool KanashiiFlag
    {
        get { return kanashiiFlag; }  //取得用
        set { kanashiiFlag = value; } //値入力用
    }

    public bool ObieteiruFlag
    {
        get { return obieteiruFlag; }  //取得用
        set { obieteiruFlag = value; } //値入力用
    }

    public bool ShinpaishiteiruFlag
    {
        get { return shinpaishiteiruFlag; }  //取得用
        set { shinpaishiteiruFlag = value; } //値入力用
    }

    public bool KowaiFlag
    {
        get { return kowaiFlag; }  //取得用
        set { kowaiFlag = value; } //値入力用
    }

    public bool TouwakushiteiruFlag
    {
        get { return touwakushiteiruFlag; }  //取得用
        set { touwakushiteiruFlag = value; } //値入力用
    }

    public bool OchitukanaiFlag
    {
        get { return ochitukanaiFlag; }  //取得用
        set { ochitukanaiFlag = value; } //値入力用
    }

    // 悲しみの感情のsetter／getter
    public bool MikagirareteiruFlag
    {
        get { return mikagirareteiruFlag; }  //取得用
        set { mikagirareteiruFlag = value; } //値入力用
    }

    public bool WabishiiFlag
    {
        get { return wabishiiFlag; }  //取得用
        set { wabishiiFlag = value; } //値入力用
    }

    public bool RakutanshiteiruFlag
    {
        get { return rakutanshiteiruFlag; }  //取得用
        set { rakutanshiteiruFlag = value; } //値入力用
    }

    public bool KurushiiFlag
    {
        get { return kurushiiFlag; }  //取得用
        set { kurushiiFlag = value; } //値入力用
    }

    public bool YarusenaiFlag
    {
        get { return yarusenaiFlag; }  //取得用
        set { yarusenaiFlag = value; } //値入力用
    }

    public bool TuraiFlag
    {
        get { return turaiFlag; }  //取得用
        set { turaiFlag = value; } //値入力用
    }

    public bool HituunaFlag
    {
        get { return hituunaFlag; }  //取得用
        set { hituunaFlag = value; } //値入力用
    }

    public bool KodokunaFlag
    {
        get { return kodokunaFlag; }  //取得用
        set { kodokunaFlag = value; } //値入力用
    }

    public bool MijimenaFlag
    {
        get { return mijimenaFlag; }  //取得用
        set { mijimenaFlag = value; } //値入力用
    }

    public bool UchihishigareteiruFlag
    {
        get { return uchihishigareteiruFlag; }  //取得用
        set { uchihishigareteiruFlag = value; } //値入力用
    }

    public bool SabishiiFlag
    {
        get { return sabishiiFlag; }  //取得用
        set { sabishiiFlag = value; } //値入力用
    }

    public bool MunashiiFlag
    {
        get { return munashiiFlag; }  //取得用
        set { munashiiFlag = value; } //値入力用
    }

    // 驚きの感情のsetter／getter
    public bool BouzenFlag
    {
        get { return bouzenFlag; }  //取得用
        set { bouzenFlag = value; } //値入力用
    }

    public bool HoushinFlag
    {
        get { return houshinFlag; }  //取得用
        set { houshinFlag = value; } //値入力用
    }

    public bool OdorokiFlag
    {
        get { return odorokiFlag; }  //取得用
        set { odorokiFlag = value; } //値入力用
    }

    public bool GyuoutenFlag
    {
        get { return gyuoutenFlag; }  //取得用
        set { gyuoutenFlag = value; } //値入力用
    }

    public bool KyoutanFlag
    {
        get { return kyoutanFlag; }  //取得用
        set { kyoutanFlag = value; } //値入力用
    }

    public bool IgaiFlag
    {
        get { return igaiFlag; }  //取得用
        set { igaiFlag = value; } //値入力用
    }

    public bool BikkurisuruFlag
    {
        get { return bikkurisuruFlag; }  //取得用
        set { bikkurisuruFlag = value; } //値入力用
    }

    public bool ShougekiFlag
    {
        get { return shougekiFlag; }  //取得用
        set { shougekiFlag = value; } //値入力用
    }

    public bool TouwakusuruFlag
    {
        get { return touwakusuruFlag; }  //取得用
        set { touwakusuruFlag = value; } //値入力用
    }

    public bool FushinFlag
    {
        get { return fushinFlag; }  //取得用
        set { fushinFlag = value; } //値入力用
    }

    public bool KyoutendouchiFlag
    {
        get { return kyoutendouchiFlag; }  //取得用
        set { kyoutendouchiFlag = value; } //値入力用
    }

    public bool AkkenitorareruFlag
    {
        get { return akkenitorareruFlag; }  //取得用
        set { akkenitorareruFlag = value; } //値入力用
    }

    // 幸せの感情のsetter／getter
    public bool HeionnaFlag
    {
        get { return heionnaFlag; }  //取得用
        set { heionnaFlag = value; } //値入力用
    }

    public bool SiawasenayorokobiFlag
    {
        get { return siawasenayorokobiFlag; }  //取得用
        set { siawasenayorokobiFlag = value; } //値入力用
    }

    public bool UchoutennaFlag
    {
        get { return uchoutennaFlag; }  //取得用
        set { uchoutennaFlag = value; } //値入力用
    }

    public bool KankiFlag
    {
        get { return kankiFlag; }  //取得用
        set { kankiFlag = value; } //値入力用
    }

    public bool OchitukuFlag
    {
        get { return ochitukuFlag; }  //取得用
        set { ochitukuFlag = value; } //値入力用
    }

    public bool KoufukukanFlag
    {
        get { return koufukukanFlag; }  //取得用
        set { koufukukanFlag = value; } //値入力用
    }

    public bool ManzokuFlag
    {
        get { return manzokuFlag; }  //取得用
        set { manzokuFlag = value; } //値入力用
    }

    public bool UresiiFlag
    {
        get { return uresiiFlag; }  //取得用
        set { uresiiFlag = value; } //値入力用
    }

    public bool OoyorokobiFlag
    {
        get { return ooyorokobiFlag; }  //取得用
        set { ooyorokobiFlag = value; } //値入力用
    }

    public bool TanosisounaFlag
    {
        get { return tanosisounaFlag; }  //取得用
        set { tanosisounaFlag = value; } //値入力用
    }

    public bool SekkyokutekinaFlag
    {
        get { return sekkyokutekinaFlag; }  //取得用
        set { sekkyokutekinaFlag = value; } //値入力用
    }

    // 嫌悪の感情のsetter／getter
    public bool TaikutunaFlag
    {
        get { return taikutunaFlag; }  //取得用
        set { taikutunaFlag = value; } //値入力用
    }

    public bool UnzariFlag
    {
        get { return unzariFlag; }  //取得用
        set { unzariFlag = value; } //値入力用
    }

    public bool TsuyoikenoFlag
    {
        get { return tsuyoikenoFlag; }  //取得用
        set { tsuyoikenoFlag = value; } //値入力用
    }

    public bool BatousuruFlag
    {
        get { return batousuruFlag; }  //取得用
        set { batousuruFlag = value; } //値入力用
    }

    public bool HidokukirauFlag
    {
        get { return hidokukirauFlag; }  //取得用
        set { hidokukirauFlag = value; } //値入力用
    }

    public bool IyakeFlag
    {
        get { return iyakeFlag; }  //取得用
        set { iyakeFlag = value; } //値入力用
    }

    public bool DoutokuseinonaiFlag
    {
        get { return doutokuseinonaiFlag; }  //取得用
        set { doutokuseinonaiFlag = value; } //値入力用
    }

    public bool ZottosuruFlag
    {
        get { return zottosuruFlag; }  //取得用
        set { zottosuruFlag = value; } //値入力用
    }

    public bool UzaiFlag
    {
        get { return uzaiFlag; }  //取得用
        set { uzaiFlag = value; } //値入力用
    }

    public bool MukatsukuFlag
    {
        get { return mukatsukuFlag; }  //取得用
        set { mukatsukuFlag = value; } //値入力用
    }

    public bool ZouoFlag
    {
        get { return zouoFlag; }  //取得用
        set { zouoFlag = value; } //値入力用
    }

    public bool FukushuuFlag
    {
        get { return fukushuuFlag; }  //取得用
        set { fukushuuFlag = value; } //値入力用
    }


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
