﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene002Sound : MonoBehaviour
{
    
    public AudioClip bossSoundScene002_001;
    public AudioClip bossSoundScene002_002;
    public AudioClip bossSoundScene002_003;
    public AudioClip bossSoundScene002_004;
    public AudioClip bossSoundScene002_005;
    public AudioClip bossSoundScene002_006;
    public AudioClip bossSoundScene002_007;
    public AudioClip bossSoundScene002_008;
    public AudioClip bossSoundScene002_009;
    private AudioSource audioSourceBossSoundScene002;

    //シーンがスタートしてサンタが話始めるまでの時間
    
    public float bossSoundScene002_001StartTime = 5.0f;
    public float bossSoundScene002_002StartTime = 10.0f;
    public float bossSoundScene002_003StartTime = 15.0f;
    public float bossSoundScene002_004StartTime = 20.0f;
    public float bossSoundScene002_005StartTime = 25.0f;
    public float bossSoundScene002_006StartTime = 30.0f;
    public float bossSoundScene002_007StartTime = 35.0f;
    public float bossSoundScene002_008StartTime = 40.0f;
    public float bossSoundScene002_009StartTime = 45.0f;
    public float bossSoundScene002_010StartTime = 55.0f;

    //シーンがスタートしてシーン３に切り替わるまでの時間
    public float scene003ChangeStartTime = 30.0f;

    // Start is called before the first frame update
    void Start()
    {
        audioSourceBossSoundScene002 = gameObject.GetComponent<AudioSource>();

        

        Invoke("StartBossSoundScene002_001", bossSoundScene002_001StartTime);
        Invoke("StartBossSoundScene002_002", bossSoundScene002_002StartTime);
        Invoke("StartBossSoundScene002_003", bossSoundScene002_003StartTime);
        Invoke("StartBossSoundScene002_004", bossSoundScene002_004StartTime);
        Invoke("StartBossSoundScene002_005", bossSoundScene002_005StartTime);
        Invoke("StartBossSoundScene002_006", bossSoundScene002_006StartTime);
        Invoke("StartBossSoundScene002_007", bossSoundScene002_007StartTime);
        Invoke("StartBossSoundScene002_008", bossSoundScene002_008StartTime);
        Invoke("StartBossSoundScene002_009", bossSoundScene002_009StartTime);
        Invoke("StartBossSoundScene002_010", bossSoundScene002_010StartTime);
        Invoke("StartScene003Change", scene003ChangeStartTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    //ボスの声再生
    void StartBossSoundScene002_001()
    {
        audioSourceBossSoundScene002.PlayOneShot(bossSoundScene002_001);
    }
    void StartBossSoundScene002_002()
    {
        audioSourceBossSoundScene002.PlayOneShot(bossSoundScene002_002);
    }
    void StartBossSoundScene002_003()
    {
        audioSourceBossSoundScene002.PlayOneShot(bossSoundScene002_003);
    }
    void StartBossSoundScene002_004()
    {
        audioSourceBossSoundScene002.PlayOneShot(bossSoundScene002_004);
    }
    void StartBossSoundScene002_005()
    {
        audioSourceBossSoundScene002.PlayOneShot(bossSoundScene002_005);
    }
    void StartBossSoundScene002_006()
    {
        audioSourceBossSoundScene002.PlayOneShot(bossSoundScene002_006);
    }
    void StartBossSoundScene002_007()
    {
        audioSourceBossSoundScene002.PlayOneShot(bossSoundScene002_007);
    }
    void StartBossSoundScene002_008()
    {
        audioSourceBossSoundScene002.PlayOneShot(bossSoundScene002_008);
    }
    void StartBossSoundScene002_009()
    {
        audioSourceBossSoundScene002.PlayOneShot(bossSoundScene002_009);
    }
    void StartScene003Change()
    {
        audioSourceBossSoundScene002.volume = 0.2f;
    }
}
