﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MovieControl : MonoBehaviour
{
    private VideoPlayer video;
    private bool playOnOffFlag = true; 

    // Start is called before the first frame update
    void Start()
    {
        video = this.GetComponent<VideoPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
//        Invoke("DelayMethod", 3.5f);

        if (OVRInput.GetDown(OVRInput.RawButton.Y))
        {
            if (OVRInput.GetDown(OVRInput.RawButton.Y))
            {
                if (playOnOffFlag)
                {
                    video.Stop();
                    playOnOffFlag = false;
                }
                else
                {
                    video.Play();
                    playOnOffFlag = true;
                }
                
            }
        }
    }

    void DelayMethod()
    {
        video.Play();
    }
}
