﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeSceneByButtonScene004 : MonoBehaviour
{
    public string MoveSceneByButton;
    public GameObject Explain001;
    public GameObject Explain002;
    public GameObject Explain003;
    private bool ChangeSceneFlg001 = false;
    private bool ChangeSceneFlg002 = false;
    private bool ChangeSceneFlg003 = false;
    private bool ChangeSceneFlg004 = false;
    private bool ChangeSceneFlg005 = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnClick()
    {
        if (ChangeSceneFlg001)
        {
            SceneManager.LoadScene(MoveSceneByButton);
        }
        else
        {
            Explain001.SetActive(false);
            Explain002.SetActive(false);
            Explain003.SetActive(true);
            ChangeSceneFlg001 = true;
        }
    }
}
