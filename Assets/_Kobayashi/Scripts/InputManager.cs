﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour
{
    StateManager stateManager = new StateManager();

    [SerializeField] GameObject rightController;
    [SerializeField] GameObject leftController;
    [SerializeField] LineRenderer rayObject;

    // 最初の説明
    [SerializeField] GameObject Explain_000;

    // 感情のオブジェクト
    [SerializeField] GameObject Emotion_001;
    [SerializeField] GameObject Emotion_002_00_osore;
    [SerializeField] GameObject Emotion_002_01_ikari;
    [SerializeField] GameObject Emotion_002_02_kanashimi;
    [SerializeField] GameObject Emotion_002_03_odoroki;
    [SerializeField] GameObject Emotion_002_04_shiawase;
    [SerializeField] GameObject Emotion_002_05_keno;


    private GameObject GetKanjyoObject;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //レイを出す処理
        rayObject.SetVertexCount(2); //頂点数を2個に設定（始点と終点）
        rayObject.SetPosition(0, rightController.transform.position); // 0番目の頂点を左手コントローラの位置に設定
        rayObject.SetPosition(1, rightController.transform.position + rightController.transform.forward * 100.0f); // 1番目の頂点を左手コントローラの位置から100m先に設定

        rayObject.SetWidth(0.01f, 0.01f); //線の太さを0.01mに設定

        //レイに当たった物体をつかむ処理
        if (OVRInput.GetDown(OVRInput.RawButton.A))
        {
            RaycastHit[] hits;
            hits = Physics.RaycastAll(rightController.transform.position, rightController.transform.forward, 100.0f);
            foreach (var hit in hits)
            {
                // 最初の説明部分
                if (hit.collider.tag == "000_next")
                {
                    Debug.Log("000_next");
                    Explain_000.SetActive(false);
                    Emotion_001.SetActive(true);
                }

                // メインの感情
                if (hit.collider.tag == "001_osore")
                {
                    Debug.Log("001_osore");
                    Emotion_001.SetActive(false);
                    Emotion_002_00_osore.SetActive(true);
                }
                if (hit.collider.tag == "001_ikari")
                {
                    Debug.Log("001_ikari");
                    Emotion_001.SetActive(false);
                    Emotion_002_01_ikari.SetActive(true);
                }
                if (hit.collider.tag == "001_kanashimi")
                {
                    Debug.Log("001_kanashimi");
                    Emotion_001.SetActive(false);
                    Emotion_002_02_kanashimi.SetActive(true);
                }
                if (hit.collider.tag == "001_odoroki")
                {
                    Debug.Log("001_odoroki");
                    Emotion_001.SetActive(false);
                    Emotion_002_03_odoroki.SetActive(true);
                }
                if (hit.collider.tag == "001_shiawase")
                {
                    Debug.Log("001_shiawase");
                    Emotion_001.SetActive(false);
                    Emotion_002_04_shiawase.SetActive(true);
                }
                if (hit.collider.tag == "001_keno")
                {
                    Debug.Log("001_keno");
                    Emotion_001.SetActive(false);
                    Emotion_002_05_keno.SetActive(true);
                }

                // 怒りの感情
                if (hit.collider.tag == "002_00_roubaishiteiru")
                {
                    Debug.Log("002_00_roubaishiteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_roubaishiteiru");
                    if (stateManager.RoubaishiteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.RoubaishiteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.RoubaishiteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_douyoushiteiru")
                {
                    Debug.Log("002_00_douyoushiteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_douyoushiteiru");
                    if (stateManager.DouyoushiteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.DouyoushiteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.DouyoushiteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_ushirometai")
                {
                    Debug.Log("002_00_ushirometai");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_ushirometai");
                    if (stateManager.UshirometaiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.UshirometaiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.UshirometaiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_menkuratteiru")
                {
                    Debug.Log("002_00_menkuratteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_menkuratteiru");
                    if (stateManager.MenkuratteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.MenkuratteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.MenkuratteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_bikkurishiteiru")
                {
                    Debug.Log("002_00_bikkurishiteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_bikkurishiteiru");
                    if (stateManager.BikkurishiteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.BikkurishiteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.BikkurishiteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_tohounikureteiru")
                {
                    Debug.Log("002_00_tohounikureteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_tohounikureteiru");
                    if (stateManager.TohounikureteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.TohounikureteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.TohounikureteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_kanashii")
                {
                    Debug.Log("002_00_kanashii");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_kanashii");
                    if (stateManager.KanashiiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KanashiiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KanashiiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_obieteiru")
                {
                    Debug.Log("002_00_obieteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_obieteiru");
                    if (stateManager.ObieteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.ObieteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.ObieteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_shinpaishiteiru")
                {
                    Debug.Log("002_00_shinpaishiteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_shinpaishiteiru");
                    if (stateManager.ShinpaishiteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.ShinpaishiteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.ShinpaishiteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_kowai")
                {
                    Debug.Log("002_00_kowai");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_kowai");
                    if (stateManager.KowaiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KowaiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KowaiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_touwakushiteiru")
                {
                    Debug.Log("002_00_touwakushiteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_touwakushiteiru");
                    if (stateManager.TouwakushiteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.TouwakushiteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.TouwakushiteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_ochitukanai")
                {
                    Debug.Log("002_00_ochitukanai");
                    GetKanjyoObject = GameObject.FindWithTag("002_00_ochitukanai");
                    if (stateManager.OchitukanaiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.OchitukanaiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.OchitukanaiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_00_back")
                {
                    Debug.Log("002_00_back");
                    Emotion_002_00_osore.SetActive(false);
                    Emotion_001.SetActive(true);
                }
                if (hit.collider.tag == "002_00_decide")
                {
                    Debug.Log("002_00_decide");
                    SceneManager.LoadScene("Scene004_ChoisedEmotions");
                }

                // 怒りの感情
                if (hit.collider.tag == "002_01_nageyarida")
                {
                    Debug.Log("002_01_nageyarida");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_nageyarida");
                    if (stateManager.NageyaridaFlag) {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.NageyaridaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.NageyaridaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_suneteiru")
                {
                    Debug.Log("002_01_suneteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_suneteiru");
                    if (stateManager.SuneteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.SuneteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.SuneteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_jiboujikininatteiru")
                {
                    Debug.Log("002_01_jiboujikininatteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_jiboujikininatteiru");
                    if (stateManager.JiboujikininatteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.JiboujikininatteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.JiboujikininatteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_kattoshiteiru")
                {
                    Debug.Log("002_01_kattoshiteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_kattoshiteiru");
                    if (stateManager.KattoshiteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KattoshiteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KattoshiteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_shitsuboushiteiru")
                {
                    Debug.Log("002_01_shitsuboushiteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_shitsuboushiteiru");
                    if (stateManager.ShitsuboushiteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.ShitsuboushiteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.ShitsuboushiteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_fuyukaidearu")
                {
                    Debug.Log("002_01_fuyukaidearu");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_fuyukaidearu");
                    if (stateManager.FuyukaidearuFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.FuyukaidearuFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.FuyukaidearuFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_okotteiru")
                {
                    Debug.Log("002_01_okotteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_okotteiru");
                    if (stateManager.OkotteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.OkotteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.OkotteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_kireru")
                {
                    Debug.Log("002_01_kireru");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_kireru");
                    if (stateManager.KireruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KireruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KireruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_nikui")
                {
                    Debug.Log("002_01_nikui");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_nikui");
                    if (stateManager.NikuiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.NikuiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.NikuiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_irairashita")
                {
                    Debug.Log("002_01_irairashita");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_irairashita");
                    if (stateManager.IrairashitaFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.IrairashitaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.IrairashitaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_wazurawashii")
                {
                    Debug.Log("002_01_wazurawashii");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_wazurawashii");
                    if (stateManager.WazurawashiiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.WazurawashiiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.WazurawashiiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_haratadashii")
                {
                    Debug.Log("002_01_haratadashii");
                    GetKanjyoObject = GameObject.FindWithTag("002_01_haratadashii");
                    if (stateManager.HaratadashiiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.HaratadashiiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.HaratadashiiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_01_back")
                {
                    Debug.Log("002_01_back");
                    Emotion_002_01_ikari.SetActive(false);
                    Emotion_001.SetActive(true);
                }
                if (hit.collider.tag == "002_01_decide")
                {
                    Debug.Log("002_01_decide");
                    SceneManager.LoadScene("Scene004_ChoisedEmotions");
                }

                // 悲しみの感情
                if (hit.collider.tag == "002_02_mikagirareteiru")
                {
                    Debug.Log("002_02_mikagirareteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_mikagirareteiru");
                    if (stateManager.MikagirareteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.MikagirareteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.MikagirareteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_wabishii")
                {
                    Debug.Log("002_02_wabishii");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_wabishii");
                    if (stateManager.WabishiiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.WabishiiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.WabishiiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_rakutanshiteiru")
                {
                    Debug.Log("002_02_rakutanshiteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_rakutanshiteiru");
                    if (stateManager.RakutanshiteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.RakutanshiteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.RakutanshiteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_kurushii")
                {
                    Debug.Log("002_02_kurushii");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_kurushii");
                    if (stateManager.KurushiiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KurushiiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KurushiiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_yarusenai")
                {
                    Debug.Log("002_02_yarusenai");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_yarusenai");
                    if (stateManager.YarusenaiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.YarusenaiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.YarusenaiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_turai")
                {
                    Debug.Log("002_02_turai");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_turai");
                    if (stateManager.TuraiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.TuraiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.TuraiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_hituuna")
                {
                    Debug.Log("002_02_hituuna");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_hituuna");
                    if (stateManager.HituunaFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.HituunaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.HituunaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_kodokuna")
                {
                    Debug.Log("002_02_kodokuna");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_kodokuna");
                    if (stateManager.KodokunaFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KodokunaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KodokunaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_mijimena")
                {
                    Debug.Log("002_02_mijimena");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_mijimena");
                    if (stateManager.MijimenaFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.MijimenaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.MijimenaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_uchihishigareteiru")
                {
                    Debug.Log("002_02_uchihishigareteiru");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_uchihishigareteiru");
                    if (stateManager.UchihishigareteiruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.UchihishigareteiruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.UchihishigareteiruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_sabishii")
                {
                    Debug.Log("002_02_sabishii");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_sabishii");
                    if (stateManager.SabishiiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.SabishiiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.SabishiiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_munashii")
                {
                    Debug.Log("002_02_munashii");
                    GetKanjyoObject = GameObject.FindWithTag("002_02_munashii");
                    if (stateManager.MunashiiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.MunashiiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.MunashiiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_02_back")
                {
                    Debug.Log("002_02_back");
                    Emotion_002_02_kanashimi.SetActive(false);
                    Emotion_001.SetActive(true);
                }
                if (hit.collider.tag == "002_02_decide")
                {
                    Debug.Log("002_02_decide");
                    SceneManager.LoadScene("Scene004_ChoisedEmotions");
                }

                // 驚きの感情
                if (hit.collider.tag == "002_03_bouzen")
                {
                    Debug.Log("002_03_bouzen");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_bouzen");
                    if (stateManager.BouzenFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.BouzenFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.BouzenFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_houshin")
                {
                    Debug.Log("002_03_houshin");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_houshin");
                    if (stateManager.HoushinFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.HoushinFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.HoushinFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_odoroki")
                {
                    Debug.Log("002_03_odoroki");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_odoroki");
                    if (stateManager.OdorokiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.OdorokiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.OdorokiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_gyuouten")
                {
                    Debug.Log("002_03_gyuouten");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_gyuouten");
                    if (stateManager.GyuoutenFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.GyuoutenFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.GyuoutenFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_kyoutan")
                {
                    Debug.Log("002_03_kyoutan");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_kyoutan");
                    if (stateManager.KyoutanFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KyoutanFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KyoutanFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_igai")
                {
                    Debug.Log("002_03_igai");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_igai");
                    if (stateManager.IgaiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.IgaiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.IgaiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_bikkurisuru")
                {
                    Debug.Log("002_03_bikkurisuru");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_bikkurisuru");
                    if (stateManager.BikkurisuruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.BikkurisuruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.BikkurisuruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_shougeki")
                {
                    Debug.Log("002_03_shougeki");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_shougeki");
                    if (stateManager.ShougekiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.ShougekiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.ShougekiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_touwakusuru")
                {
                    Debug.Log("002_03_touwakusuru");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_touwakusuru");
                    if (stateManager.TouwakusuruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.TouwakusuruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.TouwakusuruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_fushin")
                {
                    Debug.Log("002_03_fushin");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_fushin");
                    if (stateManager.FushinFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.FushinFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.FushinFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_kyoutendouchi")
                {
                    Debug.Log("002_03_kyoutendouchi");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_kyoutendouchi");
                    if (stateManager.KyoutendouchiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KyoutendouchiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KyoutendouchiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_akkenitorareru")
                {
                    Debug.Log("002_03_akkenitorareru");
                    GetKanjyoObject = GameObject.FindWithTag("002_03_akkenitorareru");
                    if (stateManager.AkkenitorareruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.AkkenitorareruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.AkkenitorareruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_03_back")
                {
                    Debug.Log("002_03_back");
                    Emotion_002_03_odoroki.SetActive(false);
                    Emotion_001.SetActive(true);
                }
                if (hit.collider.tag == "002_03_decide")
                {
                    Debug.Log("002_03_decide");
                    SceneManager.LoadScene("Scene004_ChoisedEmotions");
                }

                // 幸せの感情
                if (hit.collider.tag == "002_04_heionna")
                {
                    Debug.Log("002_04_heionna");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_heionna");
                    if (stateManager.HeionnaFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.HeionnaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.HeionnaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_siawasenayorokobi")
                {
                    Debug.Log("002_04_siawasenayorokobi");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_siawasenayorokobi");
                    if (stateManager.SiawasenayorokobiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.SiawasenayorokobiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.SiawasenayorokobiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_uchoutenna")
                {
                    Debug.Log("002_04_uchoutenna");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_uchoutenna");
                    if (stateManager.UchoutennaFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.UchoutennaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.UchoutennaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_kanki")
                {
                    Debug.Log("002_04_kanki");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_kanki");
                    if (stateManager.KankiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KankiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KankiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_ochituku")
                {
                    Debug.Log("002_04_ochituku");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_ochituku");
                    if (stateManager.OchitukuFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.OchitukuFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.OchitukuFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_koufukukan")
                {
                    Debug.Log("002_04_koufukukan");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_koufukukan");
                    if (stateManager.KoufukukanFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.KoufukukanFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.KoufukukanFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_manzoku")
                {
                    Debug.Log("002_04_manzoku");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_manzoku");
                    if (stateManager.ManzokuFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.ManzokuFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.ManzokuFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_uresii")
                {
                    Debug.Log("002_04_uresii");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_uresii");
                    if (stateManager.UresiiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.UresiiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.UresiiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_ooyorokobi")
                {
                    Debug.Log("002_04_ooyorokobi");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_ooyorokobi");
                    if (stateManager.OoyorokobiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.OoyorokobiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.OoyorokobiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_tanosisouna")
                {
                    Debug.Log("002_04_tanosisouna");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_tanosisouna");
                    if (stateManager.TanosisounaFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.TanosisounaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.TanosisounaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_sekkyokutekina")
                {
                    Debug.Log("002_04_sekkyokutekina");
                    GetKanjyoObject = GameObject.FindWithTag("002_04_sekkyokutekina");
                    if (stateManager.SekkyokutekinaFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.SekkyokutekinaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.SekkyokutekinaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_04_back")
                {
                    Debug.Log("002_04_back");
                    Emotion_002_04_shiawase.SetActive(false);
                    Emotion_001.SetActive(true);
                }
                if (hit.collider.tag == "002_04_decide")
                {
                    Debug.Log("002_04_decide");
                    SceneManager.LoadScene("Scene004_ChoisedEmotions");
                }

                // 嫌悪の感情
                if (hit.collider.tag == "002_05_taikutuna")
                {
                    Debug.Log("002_05_taikutuna");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_taikutuna");
                    if (stateManager.TaikutunaFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.TaikutunaFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.TaikutunaFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_unzari")
                {
                    Debug.Log("002_05_unzari");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_unzari");
                    if (stateManager.UnzariFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.UnzariFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.UnzariFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_tsuyoikeno")
                {
                    Debug.Log("002_05_tsuyoikeno");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_tsuyoikeno");
                    if (stateManager.TsuyoikenoFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.TsuyoikenoFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.TsuyoikenoFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_batousuru")
                {
                    Debug.Log("002_05_batousuru");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_batousuru");
                    if (stateManager.BatousuruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.BatousuruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.BatousuruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_hidokukirau")
                {
                    Debug.Log("002_05_hidokukirau");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_hidokukirau");
                    if (stateManager.HidokukirauFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.HidokukirauFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.HidokukirauFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_iyake")
                {
                    Debug.Log("002_05_iyake");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_iyake");
                    if (stateManager.IyakeFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.IyakeFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.IyakeFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_doutokuseinonai")
                {
                    Debug.Log("002_05_doutokuseinonai");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_doutokuseinonai");
                    if (stateManager.DoutokuseinonaiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.DoutokuseinonaiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.DoutokuseinonaiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_zottosuru")
                {
                    Debug.Log("002_05_zottosuru");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_zottosuru");
                    if (stateManager.ZottosuruFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.ZottosuruFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.ZottosuruFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_uzai")
                {
                    Debug.Log("002_05_uzai");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_uzai");
                    if (stateManager.UzaiFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.UzaiFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.UzaiFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_mukatsuku")
                {
                    Debug.Log("002_05_mukatsuku");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_mukatsuku");
                    if (stateManager.MukatsukuFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.MukatsukuFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.MukatsukuFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_zouo")
                {
                    Debug.Log("002_05_zouo");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_zouo");
                    if (stateManager.ZouoFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.ZouoFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.ZouoFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_fukushuu")
                {
                    Debug.Log("002_05_fukushuu");
                    GetKanjyoObject = GameObject.FindWithTag("002_05_fukushuu");
                    if (stateManager.FukushuuFlag)
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
                        stateManager.FukushuuFlag = false;
                    }
                    else
                    {
                        GetKanjyoObject.GetComponent<Renderer>().material.color = Color.green;
                        stateManager.FukushuuFlag = true;
                    }
                }
                if (hit.collider.tag == "002_05_back")
                {
                    Debug.Log("002_05_back");
                    Emotion_002_05_keno.SetActive(false);
                    Emotion_001.SetActive(true);
                }
                if (hit.collider.tag == "002_05_decide")
                {
                    Debug.Log("002_05_decide");
                    SceneManager.LoadScene("Scene004_ChoisedEmotions");
                }
            }
        }
    }
}