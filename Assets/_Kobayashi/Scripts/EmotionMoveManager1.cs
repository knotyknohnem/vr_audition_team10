﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionMoveManager1 : MonoBehaviour
{
    [SerializeField] GameObject EmotionShinpaishiteiru1;
    [SerializeField] GameObject EmotionTouwakusuru;
    [SerializeField] GameObject EmotionIrairashita;
    [SerializeField] GameObject EmotionYarusenai;
    [SerializeField] GameObject EmotionShinpaishiteiru2;
    [SerializeField] GameObject EmotionUnzari;
    [SerializeField] GameObject EmotionFuyukaida;
    [SerializeField] GameObject EmotionRakutanshiteiru;
    [SerializeField] GameObject EmotionKurusii;

    Animator animEmotionShinpaishiteiru1;
    Animator animEmotionTouwakusuru;
    Animator animEmotionIrairashita;
    Animator animEmotionYarusenai;
    Animator animEmotionShinpaishiteiru2;
    Animator animEmotionUnzari;
    Animator animEmotionFuyukaida;
    Animator animEmotionRakutanshiteiru;
    Animator animEmotionKurusii;

    public float EmotionShinpaishiteiru1StartTime = 4f;
    public float EmotionTouwakusuruStartTime = 14f;
    public float EmotionIrairashitaStartTime = 26f;
    public float EmotionYarusenaiStartTime = 32f;
    public float EmotionShinpaishiteiru2StartTime = 38f;
    public float EmotionUnzariStartTime = 53f;
    public float EmotionFuyukaidaStartTime = 58f;
    public float EmotionRakutanshiteiruStartTime = 75f;
    public float EmotionKurusiiStartTime = 84f;

    public AudioClip bossSoundScene005_001;
    public AudioClip bossSoundScene005_002;
    public AudioClip bossSoundScene005_003;
    public AudioClip bossSoundScene005_004;
    public AudioClip bossSoundScene005_005;
    public AudioClip bossSoundScene005_006;
    public AudioClip bossSoundScene005_007;
    public AudioClip bossSoundScene005_008;
    public AudioClip bossSoundScene005_009;
    private AudioSource audioSourceBossSoundScene005;


    // Start is called before the first frame update
    void Start()
    {
        audioSourceBossSoundScene005 = gameObject.GetComponent<AudioSource>();

        animEmotionShinpaishiteiru1 = EmotionShinpaishiteiru1.GetComponent<Animator>();
        Invoke("ChangeShinpaishiteiru1Flg", EmotionShinpaishiteiru1StartTime);

        animEmotionTouwakusuru = EmotionTouwakusuru.GetComponent<Animator>();
        Invoke("ChangeTouwakusuruFlg", EmotionTouwakusuruStartTime);

        animEmotionIrairashita = EmotionIrairashita.GetComponent<Animator>();
        Invoke("ChangeIrairashitaFlg", EmotionIrairashitaStartTime);

        animEmotionYarusenai = EmotionYarusenai.GetComponent<Animator>();
        Invoke("ChangeYarusenaiFlg", EmotionYarusenaiStartTime);

        animEmotionShinpaishiteiru2 = EmotionShinpaishiteiru2.GetComponent<Animator>();
        Invoke("ChangeShinpaishiteiru2Flg", EmotionShinpaishiteiru2StartTime);

        animEmotionUnzari = EmotionUnzari.GetComponent<Animator>();
        Invoke("ChangeUnzariFlg", EmotionUnzariStartTime);

        animEmotionFuyukaida = EmotionFuyukaida.GetComponent<Animator>();
        Invoke("ChangeFuyukaidaFlg", EmotionFuyukaidaStartTime);

        animEmotionRakutanshiteiru = EmotionRakutanshiteiru.GetComponent<Animator>();
        Invoke("ChangeRakutanshiteiruFlg", EmotionRakutanshiteiruStartTime);

        animEmotionKurusii = EmotionKurusii.GetComponent<Animator>();
        Invoke("ChangeKurusiiFlg", EmotionKurusiiStartTime);
    }

    void ChangeShinpaishiteiru1Flg()
    {
        animEmotionShinpaishiteiru1.SetBool("shinpaishiteiru1Flg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_001);
    }

    void ChangeTouwakusuruFlg()
    {
        animEmotionTouwakusuru.SetBool("touwakusuruFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_002);
    }

    void ChangeIrairashitaFlg()
    {
        animEmotionIrairashita.SetBool("irairashitaFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_003);
    }

    void ChangeYarusenaiFlg()
    {
        animEmotionYarusenai.SetBool("yarusenaiFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_004);
    }

    void ChangeShinpaishiteiru2Flg()
    {
        animEmotionShinpaishiteiru2.SetBool("shinpaishiteiru2Flg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_005);
    }
    void ChangeUnzariFlg()
    {
        animEmotionUnzari.SetBool("unzariFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_006);
    }
    void ChangeFuyukaidaFlg()
    {
        animEmotionFuyukaida.SetBool("fuyukaidaFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_007);
    }
    void ChangeRakutanshiteiruFlg()
    {
        animEmotionRakutanshiteiru.SetBool("rakutanshiteiruFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_008);
    }
    void ChangeKurusiiFlg()
    {
        animEmotionKurusii.SetBool("kurusiiFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_009);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
