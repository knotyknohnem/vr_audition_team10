﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeSceneByButtonScene006 : MonoBehaviour
{
    public GameObject Explain001;
    public GameObject Explain002;
    public GameObject Explain003;
    public GameObject Explain004;
    public GameObject Explain005;
    public GameObject ButtonScene006;
    private bool ChangeSceneFlg001 = false;
    private bool ChangeSceneFlg002 = false;
    private bool ChangeSceneFlg003 = false;
    private bool ChangeSceneFlg004 = false;
    private bool ChangeSceneFlg005 = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnClick()
    {
        if (ChangeSceneFlg001 && ChangeSceneFlg002 && ChangeSceneFlg003)
        {
            Explain004.SetActive(false);
            ButtonScene006.SetActive(false);
            Explain005.SetActive(true);
            ChangeSceneFlg004 = true;
        }
        else if (ChangeSceneFlg001 && ChangeSceneFlg002)
        {
            Explain003.SetActive(false);
            Explain004.SetActive(true);
            ChangeSceneFlg003 = true;
        }
        else if (ChangeSceneFlg001)
        {
            Explain002.SetActive(false);
            Explain003.SetActive(true);
            ChangeSceneFlg002 = true;
        }
        else
        {
            Explain001.SetActive(false);
            Explain002.SetActive(true);
            ChangeSceneFlg001 = true;

        }
    }

}
