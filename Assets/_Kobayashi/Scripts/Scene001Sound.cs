﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene001Sound : MonoBehaviour
{
    public AudioClip bossSoundScene001;
    private AudioSource audioSourceBossSoundScene001;

    //シーンがスタートしてサンタが話始めるまでの時間
    public float bossSoundScene001StartTime = 5.0f;

    //シーンがスタートしてシーン２に切り替わるまでの時間
    public float scene002ChangeStartTime = 8.0f;

    // Start is called before the first frame update
    void Start()
    {
        audioSourceBossSoundScene001 = gameObject.GetComponent<AudioSource>();
        Invoke("StartBossSoundScene001", bossSoundScene001StartTime);
        Invoke("StartScene002Change", scene002ChangeStartTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void StartBossSoundScene001()
    {
        audioSourceBossSoundScene001.PlayOneShot(bossSoundScene001);
    }

    void StartScene002Change()
    {
        SceneManager.LoadScene("Scene003");
    }
}
