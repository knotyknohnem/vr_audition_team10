﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeSceneByButton1 : MonoBehaviour
{
    public string MoveSceneByButton;
    public GameObject Game;
    public GameObject Explain002;
    public GameObject Explain003;
    private bool ChangeSceneFlg = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnClick()
    {
        if (ChangeSceneFlg)
        {
            SceneManager.LoadScene(MoveSceneByButton);
        }
        else
        {
            Game.SetActive(false);
            Explain002.SetActive(false);
            Explain003.SetActive(true);
            ChangeSceneFlg = true;
        }
    }

}
