﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine.Video;

public class InputManagerSampleTest : MonoBehaviour
{
    StateManager stateManager = new StateManager();
    StateManagerSampleTest stateManagerSampleTest = new StateManagerSampleTest();

    public GameObject VideoPlayerControl;
    private VideoPlayer videoPlayer;


    //    List<string> hoge = new List<string>();

    [SerializeField] GameObject rightController;
    [SerializeField] GameObject leftController;
    [SerializeField] LineRenderer rayObject;

    // 最初の説明
    [SerializeField] GameObject DoYouFeel;
    [SerializeField] GameObject EmotionStrongChoise;
    [SerializeField] GameObject ShowChoisedEmotionStrong;
    [SerializeField] GameObject Explain_000;

    private bool Stop1Flg = true;
    private bool Stop2Flg = true;
    private bool Stop3Flg = true;
    private bool Stop4Flg = true;
    private bool Stop5Flg = true;
    private bool Stop6Flg = true;


    private GameObject GetKanjyoObject;

    // Start is called before the first frame update
    void Start()
    {
        videoPlayer = VideoPlayerControl.GetComponent<VideoPlayer>();
        videoPlayer.Play();

    }

    // Update is called once per frame
    void Update()
    {
        if(videoPlayer.time >= 5f && Stop1Flg)
        {
            videoPlayer.Pause();
            Stop1Flg = false;
            DoYouFeel.SetActive(true);
        }

        if (videoPlayer.time >= 10f && Stop2Flg)
        {
            videoPlayer.Pause();
            Stop2Flg = false;
            DoYouFeel.SetActive(true);
        }

        if (videoPlayer.time >= 15f && Stop3Flg)
        {
            videoPlayer.Pause();
            Stop3Flg = false;
            DoYouFeel.SetActive(true);
        }

        if (videoPlayer.time >= 20f && Stop4Flg)
        {
            videoPlayer.Pause();
            Stop4Flg = false;
            DoYouFeel.SetActive(true);
        }

        if (videoPlayer.time >= 25f && Stop5Flg)
        {
            videoPlayer.Pause();
            Stop5Flg = false;
            DoYouFeel.SetActive(true);
        }

        if (videoPlayer.time >= 30f && Stop6Flg)
        {
            videoPlayer.Pause();
            Stop6Flg = false;
            DoYouFeel.SetActive(true);
        }
        //レイを出す処理
        rayObject.SetVertexCount(2); //頂点数を2個に設定（始点と終点）
        rayObject.SetPosition(0, rightController.transform.position); // 0番目の頂点を左手コントローラの位置に設定
        rayObject.SetPosition(1, rightController.transform.position + rightController.transform.forward * 100.0f); // 1番目の頂点を左手コントローラの位置から100m先に設定

        rayObject.SetWidth(0.01f, 0.01f); //線の太さを0.01mに設定

        //レイに当たった物体をつかむ処理
        if (OVRInput.GetDown(OVRInput.RawButton.A))
        {
            RaycastHit[] hits;
            hits = Physics.RaycastAll(rightController.transform.position, rightController.transform.forward, 100.0f);
            foreach (var hit in hits)
            {
                // はい
                if (hit.collider.tag == "Yes")
                {
                    Debug.Log("Yes");
                    DoYouFeel.SetActive(false);
                    EmotionStrongChoise.SetActive(true);
                }

                // いいえ
                if (hit.collider.tag == "No")
                {
                    Debug.Log("No");
                    StateManagerSampleTest.hoge.Add("0");
                    if (StateManagerSampleTest.hoge.Count() >= 5)
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(true);
                        videoPlayer.Play();
                    } else
                    {
                        DoYouFeel.SetActive(false);
                        videoPlayer.Play();
                    }
                }

                // １段階で選択
                if (hit.collider.tag == "EmotionStrong1")
                {
                    Debug.Log("EmotionStrong1");
                    StateManagerSampleTest.hoge.Add("1");
                    if (StateManagerSampleTest.hoge.Count() >= 5)
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(true);
                        videoPlayer.Play();
                    } else
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(false);
                        videoPlayer.Play();
                    }
                }

                // ２段階で選択
                if (hit.collider.tag == "EmotionStrong2")
                {
                    Debug.Log("EmotionStrong2");
                    StateManagerSampleTest.hoge.Add("2");
                    if (StateManagerSampleTest.hoge.Count() >= 5)
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(true);
                        videoPlayer.Play();
                    } else
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(false);
                        videoPlayer.Play();
                    }
                }

                // ３段階で選択
                if (hit.collider.tag == "EmotionStrong3")
                {
                    Debug.Log("EmotionStrong3");
                    StateManagerSampleTest.hoge.Add("3");
                    if (StateManagerSampleTest.hoge.Count() >= 5)
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(true);
                        videoPlayer.Play();
                    } else
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(false);
                        videoPlayer.Play();
                    }
                }

                // ４段階で選択
                if (hit.collider.tag == "EmotionStrong4")
                {
                    Debug.Log("EmotionStrong4");
                    StateManagerSampleTest.hoge.Add("4");
                    if (StateManagerSampleTest.hoge.Count() >= 5)
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(true);
                        videoPlayer.Play();
                    } else
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(false);
                        videoPlayer.Play();
                    }
                }

                // ５段階で選択
                if (hit.collider.tag == "EmotionStrong5")
                {
                    Debug.Log("EmotionStrong5");
                    StateManagerSampleTest.hoge.Add("5");
                    if (StateManagerSampleTest.hoge.Count() >= 5)
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(true);
                    } else
                    {
                        EmotionStrongChoise.SetActive(false);
                        DoYouFeel.SetActive(false);
                        ShowChoisedEmotionStrong.SetActive(false);
                        videoPlayer.Play();
                    }
                }
            }
        }
    }
}