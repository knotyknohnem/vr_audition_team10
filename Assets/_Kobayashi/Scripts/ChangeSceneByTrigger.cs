﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeSceneByTrigger : MonoBehaviour
{
    public void Event()
    {
        Debug.Log("イベント発生！");
        if (OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger))
        {
            SceneManager.LoadScene(MoveSceneByButton);
        }
    }

    public string MoveSceneByButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnClick()
    {
//        SceneManager.LoadScene(MoveSceneByButton);
    }

}
