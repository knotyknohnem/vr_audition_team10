﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoisedEmotions : MonoBehaviour
{
    public Text txt;

    // Start is called before the first frame update
    void Start()
    {
        txt.text = "";

        // 怒りの感情の表示
        if (StateManager.nageyaridaFlag)
        {
            txt.text += "「なげやりだ」\n";
        }
        if (StateManager.suneteiruFlag)
        {
            txt.text += "「すねている」\n";
        }
        if (StateManager.jiboujikininatteiruFlag)
        {
            txt.text += "「自暴自棄になっている」\n";
        }
        if (StateManager.kattoshiteiruFlag)
        {
            txt.text += "「カッとしている」\n";
        }
        if (StateManager.shitsuboushiteiruFlag)
        {
            txt.text += "「失望している」\n";
        }
        if (StateManager.fuyukaidearuFlag)
        {
            txt.text += "「不愉快である」\n";
        }
        if (StateManager.okotteiruFlag)
        {
            txt.text += "「怒っている」\n";
        }
        if (StateManager.kireruFlag)
        {
            txt.text += "「キレる」\n";
        }
        if (StateManager.nikuiFlag)
        {
            txt.text += "「憎い」\n";
        }
        if (StateManager.irairashitaFlag)
        {
            txt.text += "「いらいらした」\n";
        }
        if (StateManager.wazurawashiiFlag)
        {
            txt.text += "「わずらわしい」\n";
        }
        if (StateManager.haratadashiiFlag)
        {
            txt.text += "「腹立たしい」\n";
        }

        // 恐れの感情の表示
        if (StateManager.roubaishiteiruFlag)
        {
            txt.text += "「狼狽している」\n";
        }
        if (StateManager.douyoushiteiruFlag)
        {
            txt.text += "「動揺している」\n";
        }
        if (StateManager.ushirometaiFlag)
        {
            txt.text += "「うしろめたい」\n";
        }
        if (StateManager.menkuratteiruFlag)
        {
            txt.text += "「面食らっている」\n";
        }
        if (StateManager.bikkurishiteiruFlag)
        {
            txt.text += "「びっくりしている」\n";
        }
        if (StateManager.tohounikureteiruFlag)
        {
            txt.text += "「途方に暮れている」\n";
        }
        if (StateManager.kanashiiFlag)
        {
            txt.text += "「悲しい」\n";
        }
        if (StateManager.obieteiruFlag)
        {
            txt.text += "「おびえている」\n";
        }
        if (StateManager.shinpaishiteiruFlag)
        {
            txt.text += "「心配している」\n";
        }
        if (StateManager.kowaiFlag)
        {
            txt.text += "「怖い」\n";
        }
        if (StateManager.touwakushiteiruFlag)
        {
            txt.text += "「当惑している」\n";
        }
        if (StateManager.ochitukanaiFlag)
        {
            txt.text += "「落ち着かない」\n";
        }

        // 悲しみの感情の表示
        if (StateManager.mikagirareteiruFlag)
        {
            txt.text += "「見限られている」\n";
        }
        if (StateManager.wabishiiFlag)
        {
            txt.text += "「わびしい」\n";
        }
        if (StateManager.rakutanshiteiruFlag)
        {
            txt.text += "「落胆している」\n";
        }
        if (StateManager.kurushiiFlag)
        {
            txt.text += "「苦しい」\n";
        }
        if (StateManager.yarusenaiFlag)
        {
            txt.text += "「やるせない」\n";
        }
        if (StateManager.turaiFlag)
        {
            txt.text += "「つらい」\n";
        }
        if (StateManager.hituunaFlag)
        {
            txt.text += "「悲痛な」\n";
        }
        if (StateManager.kodokunaFlag)
        {
            txt.text += "「孤独な」\n";
        }
        if (StateManager.mijimenaFlag)
        {
            txt.text += "「みじめな」\n";
        }
        if (StateManager.uchihishigareteiruFlag)
        {
            txt.text += "「打ちひしがれている」\n";
        }
        if (StateManager.sabishiiFlag)
        {
            txt.text += "「さびしい」\n";
        }
        if (StateManager.munashiiFlag)
        {
            txt.text += "「むなしい」\n";
        }

        // 驚きの感情の表示
        if (StateManager.bouzenFlag)
        {
            txt.text += "「呆然」\n";
        }
        if (StateManager.houshinFlag)
        {
            txt.text += "「放心」\n";
        }
        if (StateManager.odorokiFlag)
        {
            txt.text += "「驚き」\n";
        }
        if (StateManager.gyuoutenFlag)
        {
            txt.text += "「仰天」\n";
        }
        if (StateManager.kyoutanFlag)
        {
            txt.text += "「驚嘆」\n";
        }
        if (StateManager.igaiFlag)
        {
            txt.text += "「意外」\n";
        }
        if (StateManager.bikkurisuruFlag)
        {
            txt.text += "「びっくりする」\n";
        }
        if (StateManager.shougekiFlag)
        {
            txt.text += "「衝撃」\n";
        }
        if (StateManager.touwakusuruFlag)
        {
            txt.text += "「当惑する」\n";
        }
        if (StateManager.fushinFlag)
        {
            txt.text += "「不信」\n";
        }
        if (StateManager.kyoutendouchiFlag)
        {
            txt.text += "「驚天動地」\n";
        }
        if (StateManager.akkenitorareruFlag)
        {
            txt.text += "「あっけにとられる」\n";
        }

        // 幸せの感情の表示
        if (StateManager.heionnaFlag)
        {
            txt.text += "「平穏な」\n";
        }
        if (StateManager.siawasenayorokobiFlag)
        {
            txt.text += "「幸せな喜び」\n";
        }
        if (StateManager.uchoutennaFlag)
        {
            txt.text += "「有頂天な」\n";
        }
        if (StateManager.kankiFlag)
        {
            txt.text += "「歓喜」\n";
        }
        if (StateManager.ochitukuFlag)
        {
            txt.text += "「落ち着く」\n";
        }
        if (StateManager.koufukukanFlag)
        {
            txt.text += "「幸福感」\n";
        }
        if (StateManager.manzokuFlag)
        {
            txt.text += "「満足」\n";
        }
        if (StateManager.uresiiFlag)
        {
            txt.text += "「嬉しい」\n";
        }
        if (StateManager.ooyorokobiFlag)
        {
            txt.text += "「大喜び」\n";
        }
        if (StateManager.tanosisounaFlag)
        {
            txt.text += "「楽しそうな」\n";
        }
        if (StateManager.sekkyokutekinaFlag)
        {
            txt.text += "「積極的な」\n";
        }

        // 嫌悪の感情の表示
        if (StateManager.taikutunaFlag)
        {
            txt.text += "「退屈」\n";
        }
        if (StateManager.unzariFlag)
        {
            txt.text += "「うんざり」\n";
        }
        if (StateManager.tsuyoikenoFlag)
        {
            txt.text += "「強い嫌悪」\n";
        }
        if (StateManager.batousuruFlag)
        {
            txt.text += "「罵倒する」\n";
        }
        if (StateManager.hidokukirauFlag)
        {
            txt.text += "「ひどく嫌う」\n";
        }
        if (StateManager.iyakeFlag)
        {
            txt.text += "「嫌気」\n";
        }
        if (StateManager.doutokuseinonaiFlag)
        {
            txt.text += "「道徳性のない」\n";
        }
        if (StateManager.zottosuruFlag)
        {
            txt.text += "「ぞっとする」\n";
        }
        if (StateManager.uzaiFlag)
        {
            txt.text += "「うざい」\n";
        }
        if (StateManager.mukatsukuFlag)
        {
            txt.text += "「むかつく」\n";
        }
        if (StateManager.zouoFlag)
        {
            txt.text += "「憎悪」\n";
        }
        if (StateManager.fukushuuFlag)
        {
            txt.text += "「復讐」\n";
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
