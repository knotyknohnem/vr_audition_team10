﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionMoveShinpaishiteiru1 : MonoBehaviour
{
    Animator anim;                      // Animatorコンポーネント

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        Invoke("ChangeShinpaishiteiru1Flg", 10f);
    }

    void ChangeShinpaishiteiru1Flg()
    {
        anim.SetBool("shinpaishiteiru1Flg", true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
