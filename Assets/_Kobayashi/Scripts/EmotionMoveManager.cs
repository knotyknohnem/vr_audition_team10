﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class EmotionMoveManager : MonoBehaviour
{
    [SerializeField] GameObject EmotionShinpaishiteiru1;
    [SerializeField] GameObject EmotionTouwakusuru;
    [SerializeField] GameObject EmotionIrairashita;
    [SerializeField] GameObject EmotionYarusenai;
    [SerializeField] GameObject EmotionShinpaishiteiru2;
    [SerializeField] GameObject EmotionUnzari;
    [SerializeField] GameObject EmotionFuyukaida;
    [SerializeField] GameObject EmotionRakutanshiteiru;
    [SerializeField] GameObject EmotionKurusii;

    Animator animEmotionShinpaishiteiru1;
    Animator animEmotionTouwakusuru;
    Animator animEmotionIrairashita;
    Animator animEmotionYarusenai;
    Animator animEmotionShinpaishiteiru2;
    Animator animEmotionUnzari;
    Animator animEmotionFuyukaida;
    Animator animEmotionRakutanshiteiru;
    Animator animEmotionKurusii;

    public float EmotionShinpaishiteiru1StartTime = 5f;
    public float EmotionTouwakusuruStartTime = 18.5f;
    public float EmotionIrairashitaStartTime = 32.5f;
    public float EmotionYarusenaiStartTime = 43f;
    public float EmotionShinpaishiteiru2StartTime = 51.5f;
    public float EmotionUnzariStartTime = 70f;
    public float EmotionFuyukaidaStartTime = 79.5f;
    public float EmotionRakutanshiteiruStartTime = 97.5f;
    public float EmotionKurusiiStartTime = 112f;

    public AudioClip bossSoundScene005_001;
    public AudioClip bossSoundScene005_002;
    public AudioClip bossSoundScene005_003;
    public AudioClip bossSoundScene005_004;
    public AudioClip bossSoundScene005_005;
    public AudioClip bossSoundScene005_006;
    public AudioClip bossSoundScene005_007;
    public AudioClip bossSoundScene005_008;
    public AudioClip bossSoundScene005_009;
    private AudioSource audioSourceBossSoundScene005;

    public GameObject VideoPlayerControl;
    private VideoPlayer videoPlayer;



    // Start is called before the first frame update
    void Start()
    {
        videoPlayer = VideoPlayerControl.GetComponent<VideoPlayer>();
        videoPlayer.Play();


        audioSourceBossSoundScene005 = gameObject.GetComponent<AudioSource>();

        animEmotionShinpaishiteiru1 = EmotionShinpaishiteiru1.GetComponent<Animator>();
        Invoke("ChangeShinpaishiteiru1Flg", EmotionShinpaishiteiru1StartTime);

        animEmotionTouwakusuru = EmotionTouwakusuru.GetComponent<Animator>();
        Invoke("ChangeTouwakusuruFlg", EmotionTouwakusuruStartTime);

        animEmotionIrairashita = EmotionIrairashita.GetComponent<Animator>();
        Invoke("ChangeIrairashitaFlg", EmotionIrairashitaStartTime);

        animEmotionYarusenai = EmotionYarusenai.GetComponent<Animator>();
        Invoke("ChangeYarusenaiFlg", EmotionYarusenaiStartTime);

        animEmotionShinpaishiteiru2 = EmotionShinpaishiteiru2.GetComponent<Animator>();
        Invoke("ChangeShinpaishiteiru2Flg", EmotionShinpaishiteiru2StartTime);

        animEmotionUnzari = EmotionUnzari.GetComponent<Animator>();
        Invoke("ChangeUnzariFlg", EmotionUnzariStartTime);

        animEmotionFuyukaida = EmotionFuyukaida.GetComponent<Animator>();
        Invoke("ChangeFuyukaidaFlg", EmotionFuyukaidaStartTime);

        animEmotionRakutanshiteiru = EmotionRakutanshiteiru.GetComponent<Animator>();
        Invoke("ChangeRakutanshiteiruFlg", EmotionRakutanshiteiruStartTime);

        animEmotionKurusii = EmotionKurusii.GetComponent<Animator>();
        Invoke("ChangeKurusiiFlg", EmotionKurusiiStartTime);
    }

    void ChangeShinpaishiteiru1Flg()
    {
        videoPlayer.Pause();
        animEmotionShinpaishiteiru1.SetBool("shinpaishiteiru1Flg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_001);
        Invoke("MovieStart", 3f);
    }

    void ChangeTouwakusuruFlg()
    {
        videoPlayer.Pause();
        animEmotionTouwakusuru.SetBool("touwakusuruFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_002);
        Invoke("MovieStart", 2f);

    }

    void ChangeIrairashitaFlg()
    {
        videoPlayer.Pause();
        animEmotionIrairashita.SetBool("irairashitaFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_003);
        Invoke("MovieStart", 5f);
    }

    void ChangeYarusenaiFlg()
    {
        videoPlayer.Pause();
        animEmotionYarusenai.SetBool("yarusenaiFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_004);
        Invoke("MovieStart", 3f);
    }

    void ChangeShinpaishiteiru2Flg()
    {
        videoPlayer.Pause();
        animEmotionShinpaishiteiru2.SetBool("shinpaishiteiru2Flg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_005);
        Invoke("MovieStart", 3f);
    }
    void ChangeUnzariFlg()
    {
        videoPlayer.Pause();
        animEmotionUnzari.SetBool("unzariFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_006);
        Invoke("MovieStart", 3f);
    }
    void ChangeFuyukaidaFlg()
    {
        videoPlayer.Pause();
        animEmotionFuyukaida.SetBool("fuyukaidaFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_007);
        Invoke("MovieStart", 2f);
    }
    void ChangeRakutanshiteiruFlg()
    {
        videoPlayer.Pause();
        animEmotionRakutanshiteiru.SetBool("rakutanshiteiruFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_008);
        Invoke("MovieStart", 6f);
    }
    void ChangeKurusiiFlg()
    {
        videoPlayer.Pause();
        animEmotionKurusii.SetBool("kurusiiFlg", true);
        audioSourceBossSoundScene005.PlayOneShot(bossSoundScene005_009);
        Invoke("MovieStart", 5f);
    }

    void MovieStart()
    {
        videoPlayer.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
