﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeSceneByButtonScene001 : MonoBehaviour
{
    public string MoveSceneByButton;
    public GameObject Explain001;
    public GameObject Explain002;
    public GameObject Explain003;
    public GameObject Explain004;
    private bool ChangeSceneFlg001 = false;
    private bool ChangeSceneFlg002 = false;
    private bool ChangeSceneFlg003 = false;
    private bool ChangeSceneFlg004 = false;
    private bool ChangeSceneFlg005 = false;

    public AudioClip SoundScene001_001;
    public AudioClip SoundScene001_002;
    public AudioClip SoundScene001_003;
    private AudioSource audioSourceSoundScene001;
//    public GameObject unitychan_LipSyncSound;


    // Start is called before the first frame update
    void Start()
    {
        audioSourceSoundScene001 = GetComponent<AudioSource>();
//        audioSourceSoundScene001 = unitychan_LipSyncSound.GetComponent<AudioSource>();
    }

public void OnClick()
    {
        if (ChangeSceneFlg001 && ChangeSceneFlg002 && ChangeSceneFlg003)
        {
            SceneManager.LoadScene(MoveSceneByButton);
        }
        else if (ChangeSceneFlg001 && ChangeSceneFlg002)
        {
            Explain003.SetActive(false);
            Explain004.SetActive(true);
            ChangeSceneFlg003 = true;
            Invoke("StartSoundScene001_003", 0f);
        }
        else if (ChangeSceneFlg001)
        {
            Explain002.SetActive(false);
            Explain003.SetActive(true);
            ChangeSceneFlg002 = true;
            Invoke("StartSoundScene001_002", 0f);
        }
        else
        {
            Explain001.SetActive(false);
            Explain002.SetActive(true);
            ChangeSceneFlg001 = true;
            Invoke("StartSoundScene001_001",0f);

        }
    }

    void StartSoundScene001_001()
    {
        audioSourceSoundScene001.clip = SoundScene001_001;
        audioSourceSoundScene001.Play();
    }
    void StartSoundScene001_002()
    {
        audioSourceSoundScene001.clip = SoundScene001_002;
        audioSourceSoundScene001.Play();
    }
    void StartSoundScene001_003()
    {
        audioSourceSoundScene001.clip = SoundScene001_003;
        audioSourceSoundScene001.Play();
    }
    void StopSoundScene001()
    {
        audioSourceSoundScene001.Stop();
    }
}
