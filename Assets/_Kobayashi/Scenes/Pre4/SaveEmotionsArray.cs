﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveEmotionsArray : MonoBehaviour
{
    // 保管用リスト
    public static List<string> emotions1 = new List<string>();
    public static List<string> emotions2 = new List<string>();
    public static List<string> emotions3 = new List<string>();
    public static List<string> emotions4 = new List<string>();
    public static List<string> emotions5 = new List<string>();


    //親リスト（子リストを格納する）
    public static List<List<string>> ChoicedEmotions_00 = new List<List<string>>();
//    public static List<List<string>> ChoicedEmotions_01 = new List<List<string>>();
//    public static List<List<string>> ChoicedEmotions_02 = new List<List<string>>();
//    public static List<List<string>> ChoicedEmotions_03 = new List<List<string>>();
//    public static List<List<string>> ChoicedEmotions_04 = new List<List<string>>();


    //子リスト（選択した感情を分類ごとに格納する)
    public static List<string> ChoisedEmotions_osore = new List<string>();
    public static List<string> ChoisedEmotions_ikari = new List<string>();
    public static List<string> ChoisedEmotions_kanashimi = new List<string>();
    public static List<string> ChoisedEmotions_odoroki = new List<string>();
    public static List<string> ChoisedEmotions_shiawase = new List<string>();
    public static List<string> ChoisedEmotions_keno = new List<string>();
    public static List<string> ChoisedEmotions_strong = new List<string>();

    private GameObject GetKanjyoObject;

    //子リストに格納された値をリセットするメソッド
    public void ResetList()
    {
        ChoisedEmotions_osore.Clear();
        ChoisedEmotions_kanashimi.Clear();
        ChoisedEmotions_ikari.Clear();
        ChoisedEmotions_odoroki.Clear();
        ChoisedEmotions_shiawase.Clear();
        ChoisedEmotions_keno.Clear();
        ChoisedEmotions_strong.Clear();
    }

    //親リストに子リストを格納するメソッド
    public void SaveList()
    {
        List<string>[] i = {
            ChoisedEmotions_osore, ChoisedEmotions_kanashimi, ChoisedEmotions_ikari, ChoisedEmotions_odoroki, ChoisedEmotions_shiawase, ChoisedEmotions_keno, ChoisedEmotions_strong };
        ChoicedEmotions_00.AddRange(i);

        Debug.Log(ChoicedEmotions_00.Count);

        for (int j = 0; j < ChoisedEmotions_ikari.Count; j++)
        {
            if (InputManagerArray.Array5Flg)
            {
                emotions5.Add(ChoisedEmotions_ikari[j]);
            }
            if (InputManagerArray.Array4Flg)
            {
                emotions4.Add(ChoisedEmotions_ikari[j]);
            }
            if (InputManagerArray.Array3Flg)
            {
                emotions3.Add(ChoisedEmotions_ikari[j]);
            }
            if (InputManagerArray.Array2Flg)
            {
                emotions2.Add(ChoisedEmotions_ikari[j]);
            }
            if (InputManagerArray.Array1Flg)
            {
                emotions1.Add(ChoisedEmotions_ikari[j]);
            }
        }
        for (int j = 0; j < ChoisedEmotions_kanashimi.Count; j++)
        {
            if (InputManagerArray.Array5Flg)
            {
                emotions5.Add(ChoisedEmotions_kanashimi[j]);
            }
            if (InputManagerArray.Array4Flg)
            {
                emotions4.Add(ChoisedEmotions_kanashimi[j]);
            }
            if (InputManagerArray.Array3Flg)
            {
                emotions3.Add(ChoisedEmotions_kanashimi[j]);
            }
            if (InputManagerArray.Array2Flg)
            {
                emotions2.Add(ChoisedEmotions_kanashimi[j]);
            }
            if (InputManagerArray.Array1Flg)
            {
                emotions1.Add(ChoisedEmotions_kanashimi[j]);
            }
        }
        for (int j = 0; j < ChoisedEmotions_odoroki.Count; j++)
        {
            if (InputManagerArray.Array5Flg)
            {
                emotions5.Add(ChoisedEmotions_odoroki[j]);
            }
            if (InputManagerArray.Array4Flg)
            {
                emotions4.Add(ChoisedEmotions_odoroki[j]);
            }
            if (InputManagerArray.Array3Flg)
            {
                emotions3.Add(ChoisedEmotions_odoroki[j]);
            }
            if (InputManagerArray.Array2Flg)
            {
                emotions2.Add(ChoisedEmotions_odoroki[j]);
            }
            if (InputManagerArray.Array1Flg)
            {
                emotions1.Add(ChoisedEmotions_odoroki[j]);
            }
        }
        for (int j = 0; j < ChoisedEmotions_osore.Count; j++)
        {
            if (InputManagerArray.Array5Flg)
            {
                emotions5.Add(ChoisedEmotions_osore[j]);
            }
            if (InputManagerArray.Array4Flg)
            {
                emotions4.Add(ChoisedEmotions_osore[j]);
            }
            if (InputManagerArray.Array3Flg)
            {
                emotions3.Add(ChoisedEmotions_osore[j]);
            }
            if (InputManagerArray.Array2Flg)
            {
                emotions2.Add(ChoisedEmotions_osore[j]);
            }
            if (InputManagerArray.Array1Flg)
            {
                emotions1.Add(ChoisedEmotions_osore[j]);
            }
        }
        for (int j = 0; j < ChoisedEmotions_shiawase.Count; j++)
        {
            if (InputManagerArray.Array5Flg)
            {
                emotions5.Add(ChoisedEmotions_shiawase[j]);
            }
            if (InputManagerArray.Array4Flg)
            {
                emotions4.Add(ChoisedEmotions_shiawase[j]);
            }
            if (InputManagerArray.Array3Flg)
            {
                emotions3.Add(ChoisedEmotions_shiawase[j]);
            }
            if (InputManagerArray.Array2Flg)
            {
                emotions2.Add(ChoisedEmotions_shiawase[j]);
            }
            if (InputManagerArray.Array1Flg)
            {
                emotions1.Add(ChoisedEmotions_shiawase[j]);
            }
        }
        for (int j = 0; j < ChoisedEmotions_keno.Count; j++)
        {
            if (InputManagerArray.Array5Flg)
            {
                emotions5.Add(ChoisedEmotions_keno[j]);
            }
            if (InputManagerArray.Array4Flg)
            {
                emotions4.Add(ChoisedEmotions_keno[j]);
            }
            if (InputManagerArray.Array3Flg)
            {
                emotions3.Add(ChoisedEmotions_keno[j]);
            }
            if (InputManagerArray.Array2Flg)
            {
                emotions2.Add(ChoisedEmotions_keno[j]);
            }
            if (InputManagerArray.Array1Flg)
            {
                emotions1.Add(ChoisedEmotions_keno[j]);
            }
        }
        for (int j = 0; j < ChoisedEmotions_strong.Count; j++)
        {
            if (InputManagerArray.Array5Flg)
            {
                emotions5.Add(ChoisedEmotions_strong[j]);
            }
            if (InputManagerArray.Array4Flg)
            {
                emotions4.Add(ChoisedEmotions_strong[j]);
            }
            if (InputManagerArray.Array3Flg)
            {
                emotions3.Add(ChoisedEmotions_strong[j]);
            }
            if (InputManagerArray.Array2Flg)
            {
                emotions2.Add(ChoisedEmotions_strong[j]);
            }
            if (InputManagerArray.Array1Flg)
            {
                emotions1.Add(ChoisedEmotions_strong[j]);
            }
        }

        ResetList();

        /*
         * 上記の方法が使えない時はこちらで
            //ChoicedEmotions_00.Add(ChoisedEmotions_osore);
            //ChoicedEmotions_00.Add(ChoisedEmotions_kanashimi);
            //ChoicedEmotions_00.Add(ChoisedEmotions_ikari);
            //ChoicedEmotions_00.Add(ChoisedEmotions_odoroki);
            //ChoicedEmotions_00.Add(ChoisedEmotions_shiawase);
            //ChoicedEmotions_00.Add(ChoisedEmotions_keno);
        
        */
    }

    //選択した感情を親リストに保存するメソッド（InputManegerクラスのdecide処理の際に呼び出し）
    public void AddList()
    { 

        if (StateManager.nageyaridaFlag)
        {
            ChoisedEmotions_ikari.Add("なげやりだ");
            StateManager.nageyaridaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_nageyarida");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.suneteiruFlag)
        {
            ChoisedEmotions_ikari.Add("すねている");
            StateManager.suneteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_suneteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.jiboujikininatteiruFlag)
        {
            ChoisedEmotions_ikari.Add("自暴自棄になっている");
            StateManager.jiboujikininatteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_jiboujikininatteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.kattoshiteiruFlag)
        {
            ChoisedEmotions_ikari.Add("カッとしている");
            StateManager.kattoshiteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_kattoshiteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.shitsuboushiteiruFlag)
        {
            ChoisedEmotions_ikari.Add("失望している");
            StateManager.shitsuboushiteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_shitsuboushiteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.fuyukaidearuFlag)
        {
            ChoisedEmotions_ikari.Add("不愉快である");
            StateManager.fuyukaidearuFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_fuyukaidearu");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.okotteiruFlag)
        {
            ChoisedEmotions_ikari.Add("怒っている");
            StateManager.okotteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_okotteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.kireruFlag)
        {
            ChoisedEmotions_ikari.Add("キレる");
            StateManager.kireruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_kireru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.nikuiFlag)
        {
            ChoisedEmotions_ikari.Add("憎い");
            StateManager.nikuiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_nikui");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.irairashitaFlag)
        {
            ChoisedEmotions_ikari.Add("いらいらした");
            StateManager.irairashitaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_irairashita");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.wazurawashiiFlag)
        {
            ChoisedEmotions_ikari.Add("わずらわしい");
            StateManager.wazurawashiiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_wazurawashii");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.haratadashiiFlag)
        {
            ChoisedEmotions_ikari.Add("腹立たしい");
            StateManager.haratadashiiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_01_haratadashii");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }

        // 恐れの感情の表示
        if (StateManager.roubaishiteiruFlag)
        {
            ChoisedEmotions_osore.Add("狼狽している");
            StateManager.roubaishiteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_roubaishiteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.douyoushiteiruFlag)
        {
            ChoisedEmotions_osore.Add("動揺している");
            StateManager.douyoushiteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_douyoushiteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.ushirometaiFlag)
        {
            ChoisedEmotions_osore.Add("うしろめたい");
            StateManager.ushirometaiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_ushirometai");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.menkuratteiruFlag)
        {
            ChoisedEmotions_osore.Add("面食らっている");
            StateManager.menkuratteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_menkuratteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.bikkurishiteiruFlag)
        {
            ChoisedEmotions_osore.Add("びっくりしている");
            StateManager.bikkurishiteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_bikkurishiteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.tohounikureteiruFlag)
        {
            ChoisedEmotions_osore.Add("途方に暮れている");
            StateManager.tohounikureteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_tohounikureteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.kanashiiFlag)
        {
            ChoisedEmotions_osore.Add("悲しい");
            StateManager.kanashiiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_kanashii");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.obieteiruFlag)
        {
            ChoisedEmotions_osore.Add("おびえている");
            StateManager.obieteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_obieteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.shinpaishiteiruFlag)
        {
            ChoisedEmotions_osore.Add("心配している");
            StateManager.shinpaishiteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_shinpaishiteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.kowaiFlag)
        {
            ChoisedEmotions_osore.Add("怖い");
            StateManager.kowaiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_kowai");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.touwakushiteiruFlag)
        {
            ChoisedEmotions_osore.Add("当惑している");
            StateManager.touwakushiteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_touwakushiteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.ochitukanaiFlag)
        {
            ChoisedEmotions_osore.Add("落ち着かない");
            StateManager.ochitukanaiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_00_ochitukanai");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }

        // 悲しみの感情の表示
        if (StateManager.mikagirareteiruFlag)
        {
            ChoisedEmotions_kanashimi.Add("見限られている");
            StateManager.mikagirareteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_mikagirareteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.wabishiiFlag)
        {
            ChoisedEmotions_kanashimi.Add("わびしい");
            StateManager.wabishiiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_wabishii");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.rakutanshiteiruFlag)
        {
            ChoisedEmotions_kanashimi.Add("落胆している");
            StateManager.rakutanshiteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_rakutanshiteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.kurushiiFlag)
        {
            ChoisedEmotions_kanashimi.Add("苦しい");
            StateManager.kurushiiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_kurushii");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.yarusenaiFlag)
        {
            ChoisedEmotions_kanashimi.Add("やるせない");
            StateManager.yarusenaiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_yarusenai");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.turaiFlag)
        {
            ChoisedEmotions_kanashimi.Add("つらい");
            StateManager.turaiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_turai");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.hituunaFlag)
        {
            ChoisedEmotions_kanashimi.Add("悲痛な");
            StateManager.hituunaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_hituuna");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.kodokunaFlag)
        {
            ChoisedEmotions_kanashimi.Add("孤独な");
            StateManager.kodokunaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_kodokuna");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.mijimenaFlag)
        {
            ChoisedEmotions_kanashimi.Add("みじめな");
            StateManager.mijimenaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_mijimena");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.uchihishigareteiruFlag)
        {
            ChoisedEmotions_kanashimi.Add("打ちひしがれている");
            StateManager.uchihishigareteiruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_uchihishigareteiru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.sabishiiFlag)
        {
            ChoisedEmotions_kanashimi.Add("さびしい");
            StateManager.sabishiiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_sabishii");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.munashiiFlag)
        {
            ChoisedEmotions_kanashimi.Add("むなしい");
            StateManager.munashiiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_02_munashii");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }

        // 驚きの感情の表示
        if (StateManager.bouzenFlag)
        {
            ChoisedEmotions_odoroki.Add("呆然");
            StateManager.bouzenFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_bouzen");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.houshinFlag)
        {
            ChoisedEmotions_odoroki.Add("放心");
            StateManager.houshinFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_houshin");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.odorokiFlag)
        {
            ChoisedEmotions_odoroki.Add("驚き");
            StateManager.odorokiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_odoroki");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;

        }
        if (StateManager.gyuoutenFlag)
        {
            ChoisedEmotions_odoroki.Add("仰天");
            StateManager.gyuoutenFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_gyuouten");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.kyoutanFlag)
        {
            ChoisedEmotions_odoroki.Add("驚嘆");
            StateManager.kyoutanFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_kyoutan");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.igaiFlag)
        {
            ChoisedEmotions_odoroki.Add("意外");
            StateManager.igaiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_igai");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.bikkurisuruFlag)
        {
            ChoisedEmotions_odoroki.Add("びっくりする");
            StateManager.bikkurisuruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_bikkurisuru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.shougekiFlag)
        {
            ChoisedEmotions_odoroki.Add("衝撃");
            StateManager.shougekiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_shougeki");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.touwakusuruFlag)
        {
            ChoisedEmotions_odoroki.Add("当惑する");
            StateManager.touwakusuruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_touwakusuru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.fushinFlag)
        {
            ChoisedEmotions_odoroki.Add("不信");
            StateManager.fushinFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_fushin");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.kyoutendouchiFlag)
        {
            ChoisedEmotions_odoroki.Add("驚天動地");
            StateManager.kyoutendouchiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_kyoutendouchi");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.akkenitorareruFlag)
        {
            ChoisedEmotions_odoroki.Add("あっけにとられる");
            StateManager.akkenitorareruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_03_akkenitorareru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }

        // 幸せの感情の表示
        if (StateManager.heionnaFlag)
        {
            ChoisedEmotions_shiawase.Add("平穏な");
            StateManager.heionnaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_heionna");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.siawasenayorokobiFlag)
        {
            ChoisedEmotions_shiawase.Add("幸せな喜び");
            StateManager.siawasenayorokobiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_siawasenayorokobi");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.uchoutennaFlag)
        {
            ChoisedEmotions_shiawase.Add("有頂天な");
            StateManager.uchoutennaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_uchoutenna");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.kankiFlag)
        {
            ChoisedEmotions_shiawase.Add("歓喜");
            StateManager.kankiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_kanki");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.ochitukuFlag)
        {
            ChoisedEmotions_shiawase.Add("落ち着く");
            StateManager.ochitukuFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_ochituku");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.koufukukanFlag)
        {
            ChoisedEmotions_shiawase.Add("幸福感");
            StateManager.koufukukanFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_koufukukan");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.manzokuFlag)
        {
            ChoisedEmotions_shiawase.Add("満足");
            StateManager.manzokuFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_manzoku");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.uresiiFlag)
        {
            ChoisedEmotions_shiawase.Add("嬉しい");
            StateManager.uresiiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_uresii");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.ooyorokobiFlag)
        {
            ChoisedEmotions_shiawase.Add("大喜び");
            StateManager.ooyorokobiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_ooyorokobi");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.tanosisounaFlag)
        {
            ChoisedEmotions_shiawase.Add("楽しそうな");
            StateManager.tanosisounaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_tanosisouna");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.sekkyokutekinaFlag)
        {
            ChoisedEmotions_shiawase.Add("積極的な");
            StateManager.sekkyokutekinaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_04_sekkyokutekina");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }

        // 嫌悪の感情の表示
        if (StateManager.taikutunaFlag)
        {
            ChoisedEmotions_keno.Add("退屈");
            StateManager.taikutunaFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_taikutuna");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.unzariFlag)
        {
            ChoisedEmotions_keno.Add("うんざり");
            StateManager.unzariFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_unzari");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.tsuyoikenoFlag)
        {
            ChoisedEmotions_keno.Add("強い嫌悪");
            StateManager.tsuyoikenoFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_tsuyoikeno");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.batousuruFlag)
        {
            ChoisedEmotions_keno.Add("罵倒する");
            StateManager.batousuruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_batousuru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.hidokukirauFlag)
        {
            ChoisedEmotions_keno.Add("ひどく嫌う");
            StateManager.hidokukirauFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_hidokukirau");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.iyakeFlag)
        {
            ChoisedEmotions_keno.Add("嫌気");
            StateManager.iyakeFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_iyake");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.doutokuseinonaiFlag)
        {
            ChoisedEmotions_keno.Add("道徳性のない");
            StateManager.doutokuseinonaiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_doutokuseinonai");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.zottosuruFlag)
        {
            ChoisedEmotions_keno.Add("ぞっとする");
            StateManager.zottosuruFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_zottosuru");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.uzaiFlag)
        {
            ChoisedEmotions_keno.Add("うざい");
            StateManager.uzaiFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_uzai");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }
        if (StateManager.mukatsukuFlag)
        {
            ChoisedEmotions_keno.Add("むかつく");
            StateManager.mukatsukuFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_mukatsuku");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;

        }
        if (StateManager.zouoFlag)
        {
            ChoisedEmotions_keno.Add("憎悪");
            StateManager.zouoFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_zouo");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;

        }
        if (StateManager.fukushuuFlag)
        {
            ChoisedEmotions_keno.Add("復讐");
            StateManager.fukushuuFlag = false;
            GetKanjyoObject = GameObject.FindWithTag("002_05_fukushuu");
            GetKanjyoObject.GetComponent<Renderer>().material.color = Color.white;
        }

        // 感情の強さ表示
        if (StateManager.emotionStrong1Flag)
        {
            ChoisedEmotions_strong.Add("1");
            StateManager.emotionStrong1Flag = false;
        }
        if (StateManager.emotionStrong2Flag)
        {
            ChoisedEmotions_strong.Add("2");
            StateManager.emotionStrong2Flag = false;
        }
        if (StateManager.emotionStrong3Flag)
        {
            ChoisedEmotions_strong.Add("3");
            StateManager.emotionStrong3Flag = false;
        }
        if (StateManager.emotionStrong4Flag)
        {
            ChoisedEmotions_strong.Add("4");
            StateManager.emotionStrong4Flag = false;
        }
        if (StateManager.emotionStrong5Flag)
        {
            ChoisedEmotions_strong.Add("5");
            StateManager.emotionStrong5Flag = false;
        }


        SaveList();
    }

// Start is called before the first frame update
void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
