﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class EmotionMoveManagerArray : MonoBehaviour
{
    public static List<string> emotionsTest = new List<string>();

    // 各感情のオブジェクトを格納する場所
    // 怒り
    [SerializeField] GameObject EmotionNageyarida;
    [SerializeField] GameObject EmotionSuneteiru;
    [SerializeField] GameObject EmotionJiboujikininatteiru;
    [SerializeField] GameObject EmotionKattoshiteiru;
    [SerializeField] GameObject EmotionShitsuboushiteiru;
    [SerializeField] GameObject EmotionFuyukaidearu;
    [SerializeField] GameObject EmotionOkotteiru;
    [SerializeField] GameObject EmotionKireru;
    [SerializeField] GameObject EmotionNikui;
    [SerializeField] GameObject EmotionIrairashita;
    [SerializeField] GameObject EmotionWazurawashii;
    [SerializeField] GameObject EmotionhHaratadashii;
    // 恐れ
    [SerializeField] GameObject EmotionRoubaishiteiru;
    [SerializeField] GameObject EmotionDouyoushiteiru;
    [SerializeField] GameObject EmotionUshirometai;
    [SerializeField] GameObject EmotionMenkuratteiru;
    [SerializeField] GameObject EmotionBikkurishiteiru;
    [SerializeField] GameObject EmotionTohounikureteiru;
    [SerializeField] GameObject EmotionKanashii;
    [SerializeField] GameObject EmotionObieteiru;
    [SerializeField] GameObject EmotionShinpaishiteiru;
    [SerializeField] GameObject EmotionKowai;
    [SerializeField] GameObject EmotionTouwakushiteiru;
    [SerializeField] GameObject EmotionOchitukanai;
    // 悲しみ
    [SerializeField] GameObject EmotionMikagirareteiru;
    [SerializeField] GameObject EmotionWabishii;
    [SerializeField] GameObject EmotionRakutanshiteiru;
    [SerializeField] GameObject EmotionKurushii;
    [SerializeField] GameObject EmotionYarusenai;
    [SerializeField] GameObject EmotionTurai;
    [SerializeField] GameObject EmotionHituuna;
    [SerializeField] GameObject EmotionKodokuna;
    [SerializeField] GameObject EmotionMijimena;
    [SerializeField] GameObject EmotionUchihishigareteiru;
    [SerializeField] GameObject EmotionSabishii;
    [SerializeField] GameObject EmotionMunashii;
    // 驚き
    [SerializeField] GameObject EmotionBouzen;
    [SerializeField] GameObject EmotionHoushin;
    [SerializeField] GameObject EmotionOdoroki;
    [SerializeField] GameObject EmotionGyuouten;
    [SerializeField] GameObject EmotionKyoutan;
    [SerializeField] GameObject EmotionIgai;
    [SerializeField] GameObject EmotionBikkurisuru;
    [SerializeField] GameObject EmotionShougeki;
    [SerializeField] GameObject EmotionTouwakusuru;
    [SerializeField] GameObject EmotionFushin;
    [SerializeField] GameObject EmotionKyoutendouchi;
    [SerializeField] GameObject EmotionAkkenitorareru;
    // 幸せ
    [SerializeField] GameObject EmotionHeionna;
    [SerializeField] GameObject EmotionSiawasenayorokobi;
    [SerializeField] GameObject EmotionUchoutenna;
    [SerializeField] GameObject EmotionKanki;
    [SerializeField] GameObject EmotionOchituku;
    [SerializeField] GameObject EmotionKoufukukan;
    [SerializeField] GameObject EmotionManzoku;
    [SerializeField] GameObject EmotionUresii;
    [SerializeField] GameObject EmotionOoyorokobi;
    [SerializeField] GameObject EmotionTanosisouna;
    [SerializeField] GameObject EmotionSekkyokutekina;
    // 嫌悪
    [SerializeField] GameObject EmotionTaikutuna;
    [SerializeField] GameObject EmotionUnzari;
    [SerializeField] GameObject EmotionTsuyoikeno;
    [SerializeField] GameObject EmotionBatousuru;
    [SerializeField] GameObject EmotionHidokukirau;
    [SerializeField] GameObject EmotionIyake;
    [SerializeField] GameObject EmotionDoutokuseinonai;
    [SerializeField] GameObject EmotionZottosuru;
    [SerializeField] GameObject EmotionUzai;
    [SerializeField] GameObject EmotionMukatsuku;
    [SerializeField] GameObject EmotionZouo;
    [SerializeField] GameObject EmotionFukushuu;

    // アニメーション
    // 怒り
    Animator animEmotionNageyarida;
    Animator animEmotionSuneteiru;
    Animator animEmotionJiboujikininatteiru;
    Animator animEmotionKattoshiteiru;
    Animator animEmotionShitsuboushiteiru;
    Animator animEmotionFuyukaidearu;
    Animator animEmotionOkotteiru;
    Animator animEmotionKireru;
    Animator animEmotionNikui;
    Animator animEmotionIrairashita;
    Animator animEmotionWazurawashii;
    Animator animEmotionHaratadashii;
    // 恐れ
    Animator animEmotionRoubaishiteiru;
    Animator animEmotionDouyoushiteiru;
    Animator animEmotionUshirometai;
    Animator animEmotionMenkuratteiru;
    Animator animEmotionBikkurishiteiru;
    Animator animEmotionTohounikureteiru;
    Animator animEmotionKanashii;
    Animator animEmotionObieteiru;
    Animator animEmotionShinpaishiteiru;
    Animator animEmotionKowai;
    Animator animEmotionTouwakushiteiru;
    Animator animEmotionOchitukanai;
    // 悲しみ
    Animator animEmotionMikagirareteiru;
    Animator animEmotionWabishii;
    Animator animEmotionRakutanshiteiru;
    Animator animEmotionKurushii;
    Animator animEmotionYarusenai;
    Animator animEmotionTurai;
    Animator animEmotionHituuna;
    Animator animEmotionKodokuna;
    Animator animEmotionMijimena;
    Animator animEmotionUchihishigareteiru;
    Animator animEmotionSabishii;
    Animator animEmotionMunashii;
    // 驚き
    Animator animEmotionBouzen;
    Animator animEmotionHoushin;
    Animator animEmotionOdoroki;
    Animator animEmotionGyuouten;
    Animator animEmotionKyoutan;
    Animator animEmotionIgai;
    Animator animEmotionBikkurisuru;
    Animator animEmotionShougeki;
    Animator animEmotionTouwakusuru;
    Animator animEmotionFushin;
    Animator animEmotionKyoutendouchi;
    Animator animEmotionAkkenitorareru;
    // 幸せ
    Animator animEmotionHeionna;
    Animator animEmotionSiawasenayorokobi;
    Animator animEmotionUchoutenna;
    Animator animEmotionKanki;
    Animator animEmotionOchituku;
    Animator animEmotionKoufukukan;
    Animator animEmotionManzoku;
    Animator animEmotionUresii;
    Animator animEmotionOoyorokobi;
    Animator animEmotionTanosisouna;
    Animator animEmotionSekkyokutekina;
    // 嫌悪
    Animator animEmotionTaikutuna;
    Animator animEmotionUnzari;
    Animator animEmotionTsuyoikeno;
    Animator animEmotionBatousuru;
    Animator animEmotionHidokukirau;
    Animator animEmotionIyake;
    Animator animEmotionDoutokuseinonai;
    Animator animEmotionZottosuru;
    Animator animEmotionUzai;
    Animator animEmotionMukatsuku;
    Animator animEmotionZouo;
    Animator animEmotionFukushuu;

    // 停止ポイントの時間
    public float StopTime001 = 38f;
    public float StopTime002 = 84f;
    public float StopTime003 = 104f;
    public float StopTime004 = 122f;
    public float StopTime005 = 150f;
    
    // ビデオの読み込み処理
    public GameObject VideoPlayerControl;
    public VideoPlayer videoPlayer;
    public string NextMoveScene;
    private AudioSource audioSourceDoorBell;

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {

        /*
                        SaveEmotionsArray.emotions1.Add("なげやりだ");
                        SaveEmotionsArray.emotions1.Add("すねている");
                        SaveEmotionsArray.emotions1.Add("自暴自棄になっている");

                        SaveEmotionsArray.emotions2.Add("カッとしている");
                        SaveEmotionsArray.emotions2.Add("失望している");
                        SaveEmotionsArray.emotions2.Add("不愉快である");

                        SaveEmotionsArray.emotions3.Add("怒っている");
                        SaveEmotionsArray.emotions3.Add("キレる");
                        SaveEmotionsArray.emotions3.Add("憎い");

                        SaveEmotionsArray.emotions4.Add("いらいらした");
                        SaveEmotionsArray.emotions4.Add("わずらわしい");
                        SaveEmotionsArray.emotions4.Add("腹立たしい");

                        SaveEmotionsArray.emotions5.Add("狼狽している");
                        SaveEmotionsArray.emotions5.Add("動揺している");
                        SaveEmotionsArray.emotions5.Add("うしろめたい");
        */

        audioSourceDoorBell = GetComponent<AudioSource>();
        videoPlayer = VideoPlayerControl.GetComponent<VideoPlayer>();
        //        videoPlayer.Play();
        videoPlayer.loopPointReached += OnMovieFinished; // loopPointReached is the event for the end of the vide

        Debug.Log("シーン１の開始したよ");

        Invoke("PlayDoorBell", StopTime004);

        Debug.Log(SaveEmotionsArray.emotions1.Count);
        for (int a = 0; a < SaveEmotionsArray.emotions1.Count; a++)
        {
            Debug.Log(SaveEmotionsArray.emotions1[a]);
        }

        for (int a = 0; a < SaveEmotionsArray.emotions1.Count; a++)
        {
            if((SaveEmotionsArray.emotions1[a]).Contains("なげやりだ"))
            {
                Debug.Log("なげやりだ");
                animEmotionNageyarida = EmotionNageyarida.GetComponent<Animator>();
                Invoke("ChangeNageyaridaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("すねている"))
            {
                Debug.Log("すねている");
                animEmotionSuneteiru = EmotionSuneteiru.GetComponent<Animator>();
                Invoke("ChangeSuneteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("自暴自棄になっている"))
            {
                Debug.Log("自暴自棄になっている");
                animEmotionJiboujikininatteiru = EmotionJiboujikininatteiru.GetComponent<Animator>();
                Invoke("ChangeJiboujikininatteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("カッとしている"))
            {
                Debug.Log("カッとしている");
                animEmotionKattoshiteiru = EmotionKattoshiteiru.GetComponent<Animator>();
                Invoke("ChangeKattoshiteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("失望している"))
            {
                Debug.Log("失望している");
                animEmotionShitsuboushiteiru = EmotionShitsuboushiteiru.GetComponent<Animator>();
                Invoke("ChangeShitsuboushiteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("不愉快である"))
            {
                Debug.Log("不愉快である");
                animEmotionFuyukaidearu = EmotionFuyukaidearu.GetComponent<Animator>();
                Invoke("ChangeFuyukaidearuFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("怒っている"))
            {
                Debug.Log("怒っている");
                animEmotionOkotteiru = EmotionOkotteiru.GetComponent<Animator>();
                Invoke("ChangeOkotteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("キレる"))
            {
                Debug.Log("キレる");
                animEmotionKireru = EmotionKireru.GetComponent<Animator>();
                Invoke("ChangeKireruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("憎い"))
            {
                Debug.Log("憎い");
                animEmotionNikui = EmotionNikui.GetComponent<Animator>();
                Invoke("ChangeNikuiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("いらいらした"))
            {
                Debug.Log("いらいらした");
                animEmotionIrairashita = EmotionIrairashita.GetComponent<Animator>();
                Invoke("ChangeIrairashitaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("わずらわしい"))
            {
                Debug.Log("わずらわしい");
                animEmotionWazurawashii = EmotionWazurawashii.GetComponent<Animator>();
                Invoke("ChangeWazurawashiiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("腹立たしい"))
            {
                Debug.Log("腹立たしい");
                animEmotionHaratadashii = EmotionhHaratadashii.GetComponent<Animator>();
                Invoke("ChangeHaratadashiiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("狼狽している"))
            {
                Debug.Log("狼狽している");
                animEmotionRoubaishiteiru = EmotionRoubaishiteiru.GetComponent<Animator>();
                Invoke("ChangeRoubaishiteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("動揺している"))
            {
                Debug.Log("動揺している");
                animEmotionDouyoushiteiru = EmotionDouyoushiteiru.GetComponent<Animator>();
                Invoke("ChangeDouyoushiteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("うしろめたい"))
            {
                Debug.Log("うしろめたい");
                animEmotionUshirometai = EmotionUshirometai.GetComponent<Animator>();
                Invoke("ChangeUshirometaiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("面食らっている"))
            {
                Debug.Log("面食らっている");
                animEmotionMenkuratteiru = EmotionMenkuratteiru.GetComponent<Animator>();
                Invoke("ChangeMenkuratteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("びっくりしている"))
            {
                Debug.Log("びっくりしている");
                animEmotionBikkurishiteiru = EmotionBikkurishiteiru.GetComponent<Animator>();
                Invoke("ChangeBikkurishiteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("途方に暮れている"))
            {
                Debug.Log("途方に暮れている");
                animEmotionTohounikureteiru = EmotionTohounikureteiru.GetComponent<Animator>();
                Invoke("ChangeTohounikureteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("悲しい"))
            {
                Debug.Log("悲しい");
                animEmotionKanashii = EmotionKanashii.GetComponent<Animator>();
                Invoke("ChangeKanashiiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("おびえている"))
            {
                Debug.Log("おびえている");
                animEmotionObieteiru = EmotionObieteiru.GetComponent<Animator>();
                Invoke("ChangeObieteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("心配している"))
            {
                Debug.Log("心配している");
                animEmotionShinpaishiteiru = EmotionShinpaishiteiru.GetComponent<Animator>();
                Invoke("ChangeShinpaishiteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("怖い"))
            {
                Debug.Log("怖い");
                animEmotionKowai = EmotionKowai.GetComponent<Animator>();
                Invoke("ChangeKowaiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("当惑している"))
            {
                Debug.Log("当惑している");
                animEmotionTouwakushiteiru = EmotionTouwakushiteiru.GetComponent<Animator>();
                Invoke("ChangeTouwakushiteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("落ち着かない"))
            {
                Debug.Log("落ち着かない");
                animEmotionOchitukanai = EmotionOchitukanai.GetComponent<Animator>();
                Invoke("ChangeOchitukanaiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("見限られている"))
            {
                Debug.Log("見限られている");
                animEmotionMikagirareteiru = EmotionMikagirareteiru.GetComponent<Animator>();
                Invoke("ChangeMikagirareteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("わびしい"))
            {
                Debug.Log("わびしい");
                animEmotionWabishii = EmotionWabishii.GetComponent<Animator>();
                Invoke("ChangeWabishiiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("落胆している"))
            {
                Debug.Log("落胆している");
                animEmotionRakutanshiteiru = EmotionRakutanshiteiru.GetComponent<Animator>();
                Invoke("ChangeRakutanshiteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("苦しい"))
            {
                Debug.Log("苦しい");
                animEmotionKurushii = EmotionKurushii.GetComponent<Animator>();
                Invoke("ChangeKurushiiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("やるせない"))
            {
                Debug.Log("やるせない");
                animEmotionYarusenai = EmotionYarusenai.GetComponent<Animator>();
                Invoke("ChangeYarusenaiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("つらい"))
            {
                Debug.Log("つらい");
                animEmotionTurai = EmotionTurai.GetComponent<Animator>();
                Invoke("ChangeTuraiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("悲痛な"))
            {
                Debug.Log("悲痛な");
                animEmotionHituuna = EmotionHituuna.GetComponent<Animator>();
                Invoke("ChangeHituunaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("孤独な"))
            {
                Debug.Log("孤独な");
                animEmotionKodokuna = EmotionKodokuna.GetComponent<Animator>();
                Invoke("ChangeKodokunaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("みじめな"))
            {
                Debug.Log("みじめな");
                animEmotionMijimena = EmotionMijimena.GetComponent<Animator>();
                Invoke("ChangeMijimenaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("打ちひしがれている"))
            {
                Debug.Log("打ちひしがれている");
                animEmotionUchihishigareteiru = EmotionUchihishigareteiru.GetComponent<Animator>();
                Invoke("ChangeUchihishigareteiruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("さびしい"))
            {
                Debug.Log("さびしい");
                animEmotionSabishii = EmotionSabishii.GetComponent<Animator>();
                Invoke("ChangeSabishiiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("むなしい"))
            {
                Debug.Log("むなしい");
                animEmotionMunashii = EmotionMunashii.GetComponent<Animator>();
                Invoke("ChangeMunashiiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("呆然"))
            {
                Debug.Log("呆然");
                animEmotionBouzen = EmotionBouzen.GetComponent<Animator>();
                Invoke("ChangeBouzenFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("放心"))
            {
                Debug.Log("放心");
                animEmotionHoushin = EmotionHoushin.GetComponent<Animator>();
                Invoke("ChangeHoushinFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("驚き"))
            {
                Debug.Log("驚き");
                animEmotionOdoroki = EmotionOdoroki.GetComponent<Animator>();
                Invoke("ChangeOdorokiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("仰天"))
            {
                Debug.Log("仰天");
                animEmotionGyuouten = EmotionGyuouten.GetComponent<Animator>();
                Invoke("ChangeGyuoutenFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("驚嘆"))
            {
                Debug.Log("驚嘆");
                animEmotionKyoutan = EmotionKyoutan.GetComponent<Animator>();
                Invoke("ChangeKyoutanFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("意外"))
            {
                Debug.Log("意外");
                animEmotionIgai = EmotionIgai.GetComponent<Animator>();
                Invoke("ChangeIgaiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("びっくりする"))
            {
                Debug.Log("びっくりする");
                animEmotionBikkurisuru = EmotionBikkurisuru.GetComponent<Animator>();
                Invoke("ChangeBikkurisuruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("衝撃"))
            {
                Debug.Log("衝撃");
                animEmotionShougeki = EmotionShougeki.GetComponent<Animator>();
                Invoke("ChangeShougekiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("当惑する"))
            {
                Debug.Log("当惑する");
                animEmotionTouwakusuru = EmotionTouwakusuru.GetComponent<Animator>();
                Invoke("ChangeTouwakusuruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("不信"))
            {
                Debug.Log("不信");
                animEmotionFushin = EmotionFushin.GetComponent<Animator>();
                Invoke("ChangeFushinFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("驚天動地"))
            {
                Debug.Log("驚天動地");
                animEmotionKyoutendouchi = EmotionKyoutendouchi.GetComponent<Animator>();
                Invoke("ChangeKyoutendouchiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("あっけにとられる"))
            {
                Debug.Log("あっけにとられる");
                animEmotionAkkenitorareru = EmotionAkkenitorareru.GetComponent<Animator>();
                Invoke("ChangeAkkenitorareruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("平穏な"))
            {
                Debug.Log("平穏な");
                animEmotionHeionna = EmotionHeionna.GetComponent<Animator>();
                Invoke("ChangeHeionnaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("幸せな喜び"))
            {
                Debug.Log("幸せな喜び");
                animEmotionSiawasenayorokobi = EmotionSiawasenayorokobi.GetComponent<Animator>();
                Invoke("ChangeSiawasenayorokobiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("有頂天な"))
            {
                Debug.Log("有頂天な ");
                animEmotionUchoutenna = EmotionUchoutenna.GetComponent<Animator>();
                Invoke("ChangeUchoutennaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("歓喜"))
            {
                Debug.Log("歓喜");
                animEmotionKanki = EmotionKanki.GetComponent<Animator>();
                Invoke("ChangeKankiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("落ち着く"))
            {
                Debug.Log("落ち着く");
                animEmotionOchituku = EmotionOchituku.GetComponent<Animator>();
                Invoke("ChangeOchitukuFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("幸福感"))
            {
                Debug.Log("幸福感");
                animEmotionKoufukukan = EmotionKoufukukan.GetComponent<Animator>();
                Invoke("ChangeKoufukukanFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("満足"))
            {
                Debug.Log("満足");
                animEmotionManzoku = EmotionManzoku.GetComponent<Animator>();
                Invoke("ChangeManzokuFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("嬉しい"))
            {
                Debug.Log("嬉しい");
                animEmotionUresii = EmotionUresii.GetComponent<Animator>();
                Invoke("ChangeUresiiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("大喜び"))
            {
                Debug.Log("大喜び");
                animEmotionOoyorokobi = EmotionOoyorokobi.GetComponent<Animator>();
                Invoke("ChangeOoyorokobiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("楽しそうな"))
            {
                Debug.Log("楽しそうな");
                animEmotionTanosisouna = EmotionTanosisouna.GetComponent<Animator>();
                Invoke("ChangeTanosisounaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("積極的な"))
            {
                Debug.Log("積極的な");
                animEmotionSekkyokutekina = EmotionSekkyokutekina.GetComponent<Animator>();
                Invoke("ChangeSekkyokutekinaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("退屈"))
            {
                Debug.Log("退屈");
                animEmotionTaikutuna = EmotionTaikutuna.GetComponent<Animator>();
                Invoke("ChangeTaikutunaFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("うんざり"))
            {
                Debug.Log("うんざり");
                animEmotionUnzari = EmotionUnzari.GetComponent<Animator>();
                Invoke("ChangeUnzariFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("強い嫌悪"))
            {
                Debug.Log("強い嫌悪");
                animEmotionTsuyoikeno = EmotionTsuyoikeno.GetComponent<Animator>();
                Invoke("ChangeTsuyoikenoFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("罵倒する"))
            {
                Debug.Log("罵倒する");
                animEmotionBatousuru = EmotionBatousuru.GetComponent<Animator>();
                Invoke("ChangeBatousuruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("ひどく嫌う"))
            {
                Debug.Log("ひどく嫌う");
                animEmotionHidokukirau = EmotionHidokukirau.GetComponent<Animator>();
                Invoke("ChangeHidokukirauFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("嫌気"))
            {
                Debug.Log("嫌気");
                animEmotionIyake = EmotionIyake.GetComponent<Animator>();
                Invoke("ChangeIyakeFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("道徳性のない"))
            {
                Debug.Log("道徳性のない");
                animEmotionDoutokuseinonai = EmotionDoutokuseinonai.GetComponent<Animator>();
                Invoke("ChangeDoutokuseinonaiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("ぞっとする"))
            {
                Debug.Log("ぞっとする");
                animEmotionZottosuru = EmotionZottosuru.GetComponent<Animator>();
                Invoke("ChangeZottosuruFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("うざい"))
            {
                Debug.Log("うざい");
                animEmotionUzai = EmotionUzai.GetComponent<Animator>();
                Invoke("ChangeUzaiFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("むかつく"))
            {
                Debug.Log("むかつく");
                animEmotionMukatsuku = EmotionMukatsuku.GetComponent<Animator>();
                Invoke("ChangeMukatsukuFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("憎悪"))
            {
                Debug.Log("憎悪");
                animEmotionZouo = EmotionZouo.GetComponent<Animator>();
                Invoke("ChangeZouoFlg", StopTime001 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions1[a]).Contains("復讐"))
            {
                Debug.Log("復讐");
                animEmotionFukushuu = EmotionFukushuu.GetComponent<Animator>();
                Invoke("ChangeFukushuuFlg", StopTime001 + (a * 5));
            }
        }

        Debug.Log("シーン２の開始したよ");

        Debug.Log(SaveEmotionsArray.emotions2.Count);
        for (int a = 0; a < SaveEmotionsArray.emotions2.Count; a++)
        {
            Debug.Log(SaveEmotionsArray.emotions2[a]);
        }

        for (int a = 0; a < SaveEmotionsArray.emotions2.Count; a++)
        {
            if ((SaveEmotionsArray.emotions2[a]).Contains("なげやりだ"))
            {
                Debug.Log("なげやりだ");
                animEmotionNageyarida = EmotionNageyarida.GetComponent<Animator>();
                Invoke("ChangeNageyaridaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("すねている"))
            {
                Debug.Log("すねている");
                animEmotionSuneteiru = EmotionSuneteiru.GetComponent<Animator>();
                Invoke("ChangeSuneteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("自暴自棄になっている"))
            {
                Debug.Log("自暴自棄になっている");
                animEmotionJiboujikininatteiru = EmotionJiboujikininatteiru.GetComponent<Animator>();
                Invoke("ChangeJiboujikininatteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("カッとしている"))
            {
                Debug.Log("カッとしている");
                animEmotionKattoshiteiru = EmotionKattoshiteiru.GetComponent<Animator>();
                Invoke("ChangeKattoshiteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("失望している"))
            {
                Debug.Log("失望している");
                animEmotionShitsuboushiteiru = EmotionShitsuboushiteiru.GetComponent<Animator>();
                Invoke("ChangeShitsuboushiteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("不愉快である"))
            {
                Debug.Log("不愉快である");
                animEmotionFuyukaidearu = EmotionFuyukaidearu.GetComponent<Animator>();
                Invoke("ChangeFuyukaidearuFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("怒っている"))
            {
                Debug.Log("怒っている");
                animEmotionOkotteiru = EmotionOkotteiru.GetComponent<Animator>();
                Invoke("ChangeOkotteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("キレる"))
            {
                Debug.Log("キレる");
                animEmotionKireru = EmotionKireru.GetComponent<Animator>();
                Invoke("ChangeKireruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("憎い"))
            {
                Debug.Log("憎い");
                animEmotionNikui = EmotionNikui.GetComponent<Animator>();
                Invoke("ChangeNikuiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("いらいらした"))
            {
                Debug.Log("いらいらした");
                animEmotionIrairashita = EmotionIrairashita.GetComponent<Animator>();
                Invoke("ChangeIrairashitaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("わずらわしい"))
            {
                Debug.Log("わずらわしい");
                animEmotionWazurawashii = EmotionWazurawashii.GetComponent<Animator>();
                Invoke("ChangeWazurawashiiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("腹立たしい"))
            {
                Debug.Log("腹立たしい");
                animEmotionHaratadashii = EmotionhHaratadashii.GetComponent<Animator>();
                Invoke("ChangeHaratadashiiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("狼狽している"))
            {
                Debug.Log("狼狽している");
                animEmotionRoubaishiteiru = EmotionRoubaishiteiru.GetComponent<Animator>();
                Invoke("ChangeRoubaishiteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("動揺している"))
            {
                Debug.Log("動揺している");
                animEmotionDouyoushiteiru = EmotionDouyoushiteiru.GetComponent<Animator>();
                Invoke("ChangeDouyoushiteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("うしろめたい"))
            {
                Debug.Log("うしろめたい");
                animEmotionUshirometai = EmotionUshirometai.GetComponent<Animator>();
                Invoke("ChangeUshirometaiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("面食らっている"))
            {
                Debug.Log("面食らっている");
                animEmotionMenkuratteiru = EmotionMenkuratteiru.GetComponent<Animator>();
                Invoke("ChangeMenkuratteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("びっくりしている"))
            {
                Debug.Log("びっくりしている");
                animEmotionBikkurishiteiru = EmotionBikkurishiteiru.GetComponent<Animator>();
                Invoke("ChangeBikkurishiteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("途方に暮れている"))
            {
                Debug.Log("途方に暮れている");
                animEmotionTohounikureteiru = EmotionTohounikureteiru.GetComponent<Animator>();
                Invoke("ChangeTohounikureteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("悲しい"))
            {
                Debug.Log("悲しい");
                animEmotionKanashii = EmotionKanashii.GetComponent<Animator>();
                Invoke("ChangeKanashiiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("おびえている"))
            {
                Debug.Log("おびえている");
                animEmotionObieteiru = EmotionObieteiru.GetComponent<Animator>();
                Invoke("ChangeObieteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("心配している"))
            {
                Debug.Log("心配している");
                animEmotionShinpaishiteiru = EmotionShinpaishiteiru.GetComponent<Animator>();
                Invoke("ChangeShinpaishiteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("怖い"))
            {
                Debug.Log("怖い");
                animEmotionKowai = EmotionKowai.GetComponent<Animator>();
                Invoke("ChangeKowaiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("当惑している"))
            {
                Debug.Log("当惑している");
                animEmotionTouwakushiteiru = EmotionTouwakushiteiru.GetComponent<Animator>();
                Invoke("ChangeTouwakushiteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("落ち着かない"))
            {
                Debug.Log("落ち着かない");
                animEmotionOchitukanai = EmotionOchitukanai.GetComponent<Animator>();
                Invoke("ChangeOchitukanaiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("見限られている"))
            {
                Debug.Log("見限られている");
                animEmotionMikagirareteiru = EmotionMikagirareteiru.GetComponent<Animator>();
                Invoke("ChangeMikagirareteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("わびしい"))
            {
                Debug.Log("わびしい");
                animEmotionWabishii = EmotionWabishii.GetComponent<Animator>();
                Invoke("ChangeWabishiiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("落胆している"))
            {
                Debug.Log("落胆している");
                animEmotionRakutanshiteiru = EmotionRakutanshiteiru.GetComponent<Animator>();
                Invoke("ChangeRakutanshiteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("苦しい"))
            {
                Debug.Log("苦しい");
                animEmotionKurushii = EmotionKurushii.GetComponent<Animator>();
                Invoke("ChangeKurushiiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("やるせない"))
            {
                Debug.Log("やるせない");
                animEmotionYarusenai = EmotionYarusenai.GetComponent<Animator>();
                Invoke("ChangeYarusenaiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("つらい"))
            {
                Debug.Log("つらい");
                animEmotionTurai = EmotionTurai.GetComponent<Animator>();
                Invoke("ChangeTuraiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("悲痛な"))
            {
                Debug.Log("悲痛な");
                animEmotionHituuna = EmotionHituuna.GetComponent<Animator>();
                Invoke("ChangeHituunaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("孤独な"))
            {
                Debug.Log("孤独な");
                animEmotionKodokuna = EmotionKodokuna.GetComponent<Animator>();
                Invoke("ChangeKodokunaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("みじめな"))
            {
                Debug.Log("みじめな");
                animEmotionMijimena = EmotionMijimena.GetComponent<Animator>();
                Invoke("ChangeMijimenaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("打ちひしがれている"))
            {
                Debug.Log("打ちひしがれている");
                animEmotionUchihishigareteiru = EmotionUchihishigareteiru.GetComponent<Animator>();
                Invoke("ChangeUchihishigareteiruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("さびしい"))
            {
                Debug.Log("さびしい");
                animEmotionSabishii = EmotionSabishii.GetComponent<Animator>();
                Invoke("ChangeSabishiiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("むなしい"))
            {
                Debug.Log("むなしい");
                animEmotionMunashii = EmotionMunashii.GetComponent<Animator>();
                Invoke("ChangeMunashiiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("呆然"))
            {
                Debug.Log("呆然");
                animEmotionBouzen = EmotionBouzen.GetComponent<Animator>();
                Invoke("ChangeBouzenFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("放心"))
            {
                Debug.Log("放心");
                animEmotionHoushin = EmotionHoushin.GetComponent<Animator>();
                Invoke("ChangeHoushinFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("驚き"))
            {
                Debug.Log("驚き");
                animEmotionOdoroki = EmotionOdoroki.GetComponent<Animator>();
                Invoke("ChangeOdorokiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("仰天"))
            {
                Debug.Log("仰天");
                animEmotionGyuouten = EmotionGyuouten.GetComponent<Animator>();
                Invoke("ChangeGyuoutenFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("驚嘆"))
            {
                Debug.Log("驚嘆");
                animEmotionKyoutan = EmotionKyoutan.GetComponent<Animator>();
                Invoke("ChangeKyoutanFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("意外"))
            {
                Debug.Log("意外");
                animEmotionIgai = EmotionIgai.GetComponent<Animator>();
                Invoke("ChangeIgaiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("びっくりする"))
            {
                Debug.Log("びっくりする");
                animEmotionBikkurisuru = EmotionBikkurisuru.GetComponent<Animator>();
                Invoke("ChangeBikkurisuruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("衝撃"))
            {
                Debug.Log("衝撃");
                animEmotionShougeki = EmotionShougeki.GetComponent<Animator>();
                Invoke("ChangeShougekiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("当惑する"))
            {
                Debug.Log("当惑する");
                animEmotionTouwakusuru = EmotionTouwakusuru.GetComponent<Animator>();
                Invoke("ChangeTouwakusuruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("不信"))
            {
                Debug.Log("不信");
                animEmotionFushin = EmotionFushin.GetComponent<Animator>();
                Invoke("ChangeFushinFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("驚天動地"))
            {
                Debug.Log("驚天動地");
                animEmotionKyoutendouchi = EmotionKyoutendouchi.GetComponent<Animator>();
                Invoke("ChangeKyoutendouchiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("あっけにとられる"))
            {
                Debug.Log("あっけにとられる");
                animEmotionAkkenitorareru = EmotionAkkenitorareru.GetComponent<Animator>();
                Invoke("ChangeAkkenitorareruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("平穏な"))
            {
                Debug.Log("平穏な");
                animEmotionHeionna = EmotionHeionna.GetComponent<Animator>();
                Invoke("ChangeHeionnaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("幸せな喜び"))
            {
                Debug.Log("幸せな喜び");
                animEmotionSiawasenayorokobi = EmotionSiawasenayorokobi.GetComponent<Animator>();
                Invoke("ChangeSiawasenayorokobiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("有頂天な"))
            {
                Debug.Log("有頂天な ");
                animEmotionUchoutenna = EmotionUchoutenna.GetComponent<Animator>();
                Invoke("ChangeUchoutennaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("歓喜"))
            {
                Debug.Log("歓喜");
                animEmotionKanki = EmotionKanki.GetComponent<Animator>();
                Invoke("ChangeKankiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("落ち着く"))
            {
                Debug.Log("落ち着く");
                animEmotionOchituku = EmotionOchituku.GetComponent<Animator>();
                Invoke("ChangeOchitukuFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("幸福感"))
            {
                Debug.Log("幸福感");
                animEmotionKoufukukan = EmotionKoufukukan.GetComponent<Animator>();
                Invoke("ChangeKoufukukanFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("満足"))
            {
                Debug.Log("満足");
                animEmotionManzoku = EmotionManzoku.GetComponent<Animator>();
                Invoke("ChangeManzokuFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("嬉しい"))
            {
                Debug.Log("嬉しい");
                animEmotionUresii = EmotionUresii.GetComponent<Animator>();
                Invoke("ChangeUresiiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("大喜び"))
            {
                Debug.Log("大喜び");
                animEmotionOoyorokobi = EmotionOoyorokobi.GetComponent<Animator>();
                Invoke("ChangeOoyorokobiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("楽しそうな"))
            {
                Debug.Log("楽しそうな");
                animEmotionTanosisouna = EmotionTanosisouna.GetComponent<Animator>();
                Invoke("ChangeTanosisounaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("積極的な"))
            {
                Debug.Log("積極的な");
                animEmotionSekkyokutekina = EmotionSekkyokutekina.GetComponent<Animator>();
                Invoke("ChangeSekkyokutekinaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("退屈"))
            {
                Debug.Log("退屈");
                animEmotionTaikutuna = EmotionTaikutuna.GetComponent<Animator>();
                Invoke("ChangeTaikutunaFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("うんざり"))
            {
                Debug.Log("うんざり");
                animEmotionUnzari = EmotionUnzari.GetComponent<Animator>();
                Invoke("ChangeUnzariFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("強い嫌悪"))
            {
                Debug.Log("強い嫌悪");
                animEmotionTsuyoikeno = EmotionTsuyoikeno.GetComponent<Animator>();
                Invoke("ChangeTsuyoikenoFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("罵倒する"))
            {
                Debug.Log("罵倒する");
                animEmotionBatousuru = EmotionBatousuru.GetComponent<Animator>();
                Invoke("ChangeBatousuruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("ひどく嫌う"))
            {
                Debug.Log("ひどく嫌う");
                animEmotionHidokukirau = EmotionHidokukirau.GetComponent<Animator>();
                Invoke("ChangeHidokukirauFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("嫌気"))
            {
                Debug.Log("嫌気");
                animEmotionIyake = EmotionIyake.GetComponent<Animator>();
                Invoke("ChangeIyakeFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("道徳性のない"))
            {
                Debug.Log("道徳性のない");
                animEmotionDoutokuseinonai = EmotionDoutokuseinonai.GetComponent<Animator>();
                Invoke("ChangeDoutokuseinonaiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("ぞっとする"))
            {
                Debug.Log("ぞっとする");
                animEmotionZottosuru = EmotionZottosuru.GetComponent<Animator>();
                Invoke("ChangeZottosuruFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("うざい"))
            {
                Debug.Log("うざい");
                animEmotionUzai = EmotionUzai.GetComponent<Animator>();
                Invoke("ChangeUzaiFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("むかつく"))
            {
                Debug.Log("むかつく");
                animEmotionMukatsuku = EmotionMukatsuku.GetComponent<Animator>();
                Invoke("ChangeMukatsukuFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("憎悪"))
            {
                Debug.Log("憎悪");
                animEmotionZouo = EmotionZouo.GetComponent<Animator>();
                Invoke("ChangeZouoFlg", StopTime002 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions2[a]).Contains("復讐"))
            {
                Debug.Log("復讐");
                animEmotionFukushuu = EmotionFukushuu.GetComponent<Animator>();
                Invoke("ChangeFukushuuFlg", StopTime002 + (a * 5));
            }
        }

        Debug.Log("シーン３の開始したよ");

        Debug.Log(SaveEmotionsArray.emotions3.Count);
        for (int a = 0; a < SaveEmotionsArray.emotions3.Count; a++)
        {
            Debug.Log(SaveEmotionsArray.emotions3[a]);
        }

        for (int a = 0; a < SaveEmotionsArray.emotions3.Count; a++)
        {
            if ((SaveEmotionsArray.emotions3[a]).Contains("なげやりだ"))
            {
                Debug.Log("なげやりだ");
                animEmotionNageyarida = EmotionNageyarida.GetComponent<Animator>();
                Invoke("ChangeNageyaridaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("すねている"))
            {
                Debug.Log("すねている");
                animEmotionSuneteiru = EmotionSuneteiru.GetComponent<Animator>();
                Invoke("ChangeSuneteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("自暴自棄になっている"))
            {
                Debug.Log("自暴自棄になっている");
                animEmotionJiboujikininatteiru = EmotionJiboujikininatteiru.GetComponent<Animator>();
                Invoke("ChangeJiboujikininatteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("カッとしている"))
            {
                Debug.Log("カッとしている");
                animEmotionKattoshiteiru = EmotionKattoshiteiru.GetComponent<Animator>();
                Invoke("ChangeKattoshiteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("失望している"))
            {
                Debug.Log("失望している");
                animEmotionShitsuboushiteiru = EmotionShitsuboushiteiru.GetComponent<Animator>();
                Invoke("ChangeShitsuboushiteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("不愉快である"))
            {
                Debug.Log("不愉快である");
                animEmotionFuyukaidearu = EmotionFuyukaidearu.GetComponent<Animator>();
                Invoke("ChangeFuyukaidearuFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("怒っている"))
            {
                Debug.Log("怒っている");
                animEmotionOkotteiru = EmotionOkotteiru.GetComponent<Animator>();
                Invoke("ChangeOkotteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("キレる"))
            {
                Debug.Log("キレる");
                animEmotionKireru = EmotionKireru.GetComponent<Animator>();
                Invoke("ChangeKireruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("憎い"))
            {
                Debug.Log("憎い");
                animEmotionNikui = EmotionNikui.GetComponent<Animator>();
                Invoke("ChangeNikuiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("いらいらした"))
            {
                Debug.Log("いらいらした");
                animEmotionIrairashita = EmotionIrairashita.GetComponent<Animator>();
                Invoke("ChangeIrairashitaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("わずらわしい"))
            {
                Debug.Log("わずらわしい");
                animEmotionWazurawashii = EmotionWazurawashii.GetComponent<Animator>();
                Invoke("ChangeWazurawashiiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("腹立たしい"))
            {
                Debug.Log("腹立たしい");
                animEmotionHaratadashii = EmotionhHaratadashii.GetComponent<Animator>();
                Invoke("ChangeHaratadashiiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("狼狽している"))
            {
                Debug.Log("狼狽している");
                animEmotionRoubaishiteiru = EmotionRoubaishiteiru.GetComponent<Animator>();
                Invoke("ChangeRoubaishiteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("動揺している"))
            {
                Debug.Log("動揺している");
                animEmotionDouyoushiteiru = EmotionDouyoushiteiru.GetComponent<Animator>();
                Invoke("ChangeDouyoushiteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("うしろめたい"))
            {
                Debug.Log("うしろめたい");
                animEmotionUshirometai = EmotionUshirometai.GetComponent<Animator>();
                Invoke("ChangeUshirometaiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("面食らっている"))
            {
                Debug.Log("面食らっている");
                animEmotionMenkuratteiru = EmotionMenkuratteiru.GetComponent<Animator>();
                Invoke("ChangeMenkuratteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("びっくりしている"))
            {
                Debug.Log("びっくりしている");
                animEmotionBikkurishiteiru = EmotionBikkurishiteiru.GetComponent<Animator>();
                Invoke("ChangeBikkurishiteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("途方に暮れている"))
            {
                Debug.Log("途方に暮れている");
                animEmotionTohounikureteiru = EmotionTohounikureteiru.GetComponent<Animator>();
                Invoke("ChangeTohounikureteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("悲しい"))
            {
                Debug.Log("悲しい");
                animEmotionKanashii = EmotionKanashii.GetComponent<Animator>();
                Invoke("ChangeKanashiiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("おびえている"))
            {
                Debug.Log("おびえている");
                animEmotionObieteiru = EmotionObieteiru.GetComponent<Animator>();
                Invoke("ChangeObieteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("心配している"))
            {
                Debug.Log("心配している");
                animEmotionShinpaishiteiru = EmotionShinpaishiteiru.GetComponent<Animator>();
                Invoke("ChangeShinpaishiteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("怖い"))
            {
                Debug.Log("怖い");
                animEmotionKowai = EmotionKowai.GetComponent<Animator>();
                Invoke("ChangeKowaiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("当惑している"))
            {
                Debug.Log("当惑している");
                animEmotionTouwakushiteiru = EmotionTouwakushiteiru.GetComponent<Animator>();
                Invoke("ChangeTouwakushiteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("落ち着かない"))
            {
                Debug.Log("落ち着かない");
                animEmotionOchitukanai = EmotionOchitukanai.GetComponent<Animator>();
                Invoke("ChangeOchitukanaiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("見限られている"))
            {
                Debug.Log("見限られている");
                animEmotionMikagirareteiru = EmotionMikagirareteiru.GetComponent<Animator>();
                Invoke("ChangeMikagirareteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("わびしい"))
            {
                Debug.Log("わびしい");
                animEmotionWabishii = EmotionWabishii.GetComponent<Animator>();
                Invoke("ChangeWabishiiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("落胆している"))
            {
                Debug.Log("落胆している");
                animEmotionRakutanshiteiru = EmotionRakutanshiteiru.GetComponent<Animator>();
                Invoke("ChangeRakutanshiteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("苦しい"))
            {
                Debug.Log("苦しい");
                animEmotionKurushii = EmotionKurushii.GetComponent<Animator>();
                Invoke("ChangeKurushiiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("やるせない"))
            {
                Debug.Log("やるせない");
                animEmotionYarusenai = EmotionYarusenai.GetComponent<Animator>();
                Invoke("ChangeYarusenaiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("つらい"))
            {
                Debug.Log("つらい");
                animEmotionTurai = EmotionTurai.GetComponent<Animator>();
                Invoke("ChangeTuraiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("悲痛な"))
            {
                Debug.Log("悲痛な");
                animEmotionHituuna = EmotionHituuna.GetComponent<Animator>();
                Invoke("ChangeHituunaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("孤独な"))
            {
                Debug.Log("孤独な");
                animEmotionKodokuna = EmotionKodokuna.GetComponent<Animator>();
                Invoke("ChangeKodokunaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("みじめな"))
            {
                Debug.Log("みじめな");
                animEmotionMijimena = EmotionMijimena.GetComponent<Animator>();
                Invoke("ChangeMijimenaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("打ちひしがれている"))
            {
                Debug.Log("打ちひしがれている");
                animEmotionUchihishigareteiru = EmotionUchihishigareteiru.GetComponent<Animator>();
                Invoke("ChangeUchihishigareteiruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("さびしい"))
            {
                Debug.Log("さびしい");
                animEmotionSabishii = EmotionSabishii.GetComponent<Animator>();
                Invoke("ChangeSabishiiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("むなしい"))
            {
                Debug.Log("むなしい");
                animEmotionMunashii = EmotionMunashii.GetComponent<Animator>();
                Invoke("ChangeMunashiiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("呆然"))
            {
                Debug.Log("呆然");
                animEmotionBouzen = EmotionBouzen.GetComponent<Animator>();
                Invoke("ChangeBouzenFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("放心"))
            {
                Debug.Log("放心");
                animEmotionHoushin = EmotionHoushin.GetComponent<Animator>();
                Invoke("ChangeHoushinFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("驚き"))
            {
                Debug.Log("驚き");
                animEmotionOdoroki = EmotionOdoroki.GetComponent<Animator>();
                Invoke("ChangeOdorokiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("仰天"))
            {
                Debug.Log("仰天");
                animEmotionGyuouten = EmotionGyuouten.GetComponent<Animator>();
                Invoke("ChangeGyuoutenFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("驚嘆"))
            {
                Debug.Log("驚嘆");
                animEmotionKyoutan = EmotionKyoutan.GetComponent<Animator>();
                Invoke("ChangeKyoutanFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("意外"))
            {
                Debug.Log("意外");
                animEmotionIgai = EmotionIgai.GetComponent<Animator>();
                Invoke("ChangeIgaiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("びっくりする"))
            {
                Debug.Log("びっくりする");
                animEmotionBikkurisuru = EmotionBikkurisuru.GetComponent<Animator>();
                Invoke("ChangeBikkurisuruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("衝撃"))
            {
                Debug.Log("衝撃");
                animEmotionShougeki = EmotionShougeki.GetComponent<Animator>();
                Invoke("ChangeShougekiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("当惑する"))
            {
                Debug.Log("当惑する");
                animEmotionTouwakusuru = EmotionTouwakusuru.GetComponent<Animator>();
                Invoke("ChangeTouwakusuruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("不信"))
            {
                Debug.Log("不信");
                animEmotionFushin = EmotionFushin.GetComponent<Animator>();
                Invoke("ChangeFushinFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("驚天動地"))
            {
                Debug.Log("驚天動地");
                animEmotionKyoutendouchi = EmotionKyoutendouchi.GetComponent<Animator>();
                Invoke("ChangeKyoutendouchiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("あっけにとられる"))
            {
                Debug.Log("あっけにとられる");
                animEmotionAkkenitorareru = EmotionAkkenitorareru.GetComponent<Animator>();
                Invoke("ChangeAkkenitorareruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("平穏な"))
            {
                Debug.Log("平穏な");
                animEmotionHeionna = EmotionHeionna.GetComponent<Animator>();
                Invoke("ChangeHeionnaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("幸せな喜び"))
            {
                Debug.Log("幸せな喜び");
                animEmotionSiawasenayorokobi = EmotionSiawasenayorokobi.GetComponent<Animator>();
                Invoke("ChangeSiawasenayorokobiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("有頂天な"))
            {
                Debug.Log("有頂天な ");
                animEmotionUchoutenna = EmotionUchoutenna.GetComponent<Animator>();
                Invoke("ChangeUchoutennaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("歓喜"))
            {
                Debug.Log("歓喜");
                animEmotionKanki = EmotionKanki.GetComponent<Animator>();
                Invoke("ChangeKankiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("落ち着く"))
            {
                Debug.Log("落ち着く");
                animEmotionOchituku = EmotionOchituku.GetComponent<Animator>();
                Invoke("ChangeOchitukuFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("幸福感"))
            {
                Debug.Log("幸福感");
                animEmotionKoufukukan = EmotionKoufukukan.GetComponent<Animator>();
                Invoke("ChangeKoufukukanFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("満足"))
            {
                Debug.Log("満足");
                animEmotionManzoku = EmotionManzoku.GetComponent<Animator>();
                Invoke("ChangeManzokuFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("嬉しい"))
            {
                Debug.Log("嬉しい");
                animEmotionUresii = EmotionUresii.GetComponent<Animator>();
                Invoke("ChangeUresiiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("大喜び"))
            {
                Debug.Log("大喜び");
                animEmotionOoyorokobi = EmotionOoyorokobi.GetComponent<Animator>();
                Invoke("ChangeOoyorokobiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("楽しそうな"))
            {
                Debug.Log("楽しそうな");
                animEmotionTanosisouna = EmotionTanosisouna.GetComponent<Animator>();
                Invoke("ChangeTanosisounaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("積極的な"))
            {
                Debug.Log("積極的な");
                animEmotionSekkyokutekina = EmotionSekkyokutekina.GetComponent<Animator>();
                Invoke("ChangeSekkyokutekinaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("退屈"))
            {
                Debug.Log("退屈");
                animEmotionTaikutuna = EmotionTaikutuna.GetComponent<Animator>();
                Invoke("ChangeTaikutunaFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("うんざり"))
            {
                Debug.Log("うんざり");
                animEmotionUnzari = EmotionUnzari.GetComponent<Animator>();
                Invoke("ChangeUnzariFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("強い嫌悪"))
            {
                Debug.Log("強い嫌悪");
                animEmotionTsuyoikeno = EmotionTsuyoikeno.GetComponent<Animator>();
                Invoke("ChangeTsuyoikenoFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("罵倒する"))
            {
                Debug.Log("罵倒する");
                animEmotionBatousuru = EmotionBatousuru.GetComponent<Animator>();
                Invoke("ChangeBatousuruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("ひどく嫌う"))
            {
                Debug.Log("ひどく嫌う");
                animEmotionHidokukirau = EmotionHidokukirau.GetComponent<Animator>();
                Invoke("ChangeHidokukirauFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("嫌気"))
            {
                Debug.Log("嫌気");
                animEmotionIyake = EmotionIyake.GetComponent<Animator>();
                Invoke("ChangeIyakeFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("道徳性のない"))
            {
                Debug.Log("道徳性のない");
                animEmotionDoutokuseinonai = EmotionDoutokuseinonai.GetComponent<Animator>();
                Invoke("ChangeDoutokuseinonaiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("ぞっとする"))
            {
                Debug.Log("ぞっとする");
                animEmotionZottosuru = EmotionZottosuru.GetComponent<Animator>();
                Invoke("ChangeZottosuruFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("うざい"))
            {
                Debug.Log("うざい");
                animEmotionUzai = EmotionUzai.GetComponent<Animator>();
                Invoke("ChangeUzaiFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("むかつく"))
            {
                Debug.Log("むかつく");
                animEmotionMukatsuku = EmotionMukatsuku.GetComponent<Animator>();
                Invoke("ChangeMukatsukuFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("憎悪"))
            {
                Debug.Log("憎悪");
                animEmotionZouo = EmotionZouo.GetComponent<Animator>();
                Invoke("ChangeZouoFlg", StopTime003 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions3[a]).Contains("復讐"))
            {
                Debug.Log("復讐");
                animEmotionFukushuu = EmotionFukushuu.GetComponent<Animator>();
                Invoke("ChangeFukushuuFlg", StopTime003 + (a * 5));
            }
        }

        Debug.Log("シーン４の開始したよ");

        Debug.Log(SaveEmotionsArray.emotions4.Count);
        for (int a = 0; a < SaveEmotionsArray.emotions4.Count; a++)
        {
            Debug.Log(SaveEmotionsArray.emotions4[a]);
        }

        for (int a = 0; a < SaveEmotionsArray.emotions4.Count; a++)
        {
            if ((SaveEmotionsArray.emotions4[a]).Contains("なげやりだ"))
            {
                Debug.Log("なげやりだ");
                animEmotionNageyarida = EmotionNageyarida.GetComponent<Animator>();
                Invoke("ChangeNageyaridaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("すねている"))
            {
                Debug.Log("すねている");
                animEmotionSuneteiru = EmotionSuneteiru.GetComponent<Animator>();
                Invoke("ChangeSuneteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("自暴自棄になっている"))
            {
                Debug.Log("自暴自棄になっている");
                animEmotionJiboujikininatteiru = EmotionJiboujikininatteiru.GetComponent<Animator>();
                Invoke("ChangeJiboujikininatteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("カッとしている"))
            {
                Debug.Log("カッとしている");
                animEmotionKattoshiteiru = EmotionKattoshiteiru.GetComponent<Animator>();
                Invoke("ChangeKattoshiteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("失望している"))
            {
                Debug.Log("失望している");
                animEmotionShitsuboushiteiru = EmotionShitsuboushiteiru.GetComponent<Animator>();
                Invoke("ChangeShitsuboushiteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("不愉快である"))
            {
                Debug.Log("不愉快である");
                animEmotionFuyukaidearu = EmotionFuyukaidearu.GetComponent<Animator>();
                Invoke("ChangeFuyukaidearuFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("怒っている"))
            {
                Debug.Log("怒っている");
                animEmotionOkotteiru = EmotionOkotteiru.GetComponent<Animator>();
                Invoke("ChangeOkotteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("キレる"))
            {
                Debug.Log("キレる");
                animEmotionKireru = EmotionKireru.GetComponent<Animator>();
                Invoke("ChangeKireruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("憎い"))
            {
                Debug.Log("憎い");
                animEmotionNikui = EmotionNikui.GetComponent<Animator>();
                Invoke("ChangeNikuiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("いらいらした"))
            {
                Debug.Log("いらいらした");
                animEmotionIrairashita = EmotionIrairashita.GetComponent<Animator>();
                Invoke("ChangeIrairashitaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("わずらわしい"))
            {
                Debug.Log("わずらわしい");
                animEmotionWazurawashii = EmotionWazurawashii.GetComponent<Animator>();
                Invoke("ChangeWazurawashiiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("腹立たしい"))
            {
                Debug.Log("腹立たしい");
                animEmotionHaratadashii = EmotionhHaratadashii.GetComponent<Animator>();
                Invoke("ChangeHaratadashiiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("狼狽している"))
            {
                Debug.Log("狼狽している");
                animEmotionRoubaishiteiru = EmotionRoubaishiteiru.GetComponent<Animator>();
                Invoke("ChangeRoubaishiteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("動揺している"))
            {
                Debug.Log("動揺している");
                animEmotionDouyoushiteiru = EmotionDouyoushiteiru.GetComponent<Animator>();
                Invoke("ChangeDouyoushiteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("うしろめたい"))
            {
                Debug.Log("うしろめたい");
                animEmotionUshirometai = EmotionUshirometai.GetComponent<Animator>();
                Invoke("ChangeUshirometaiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("面食らっている"))
            {
                Debug.Log("面食らっている");
                animEmotionMenkuratteiru = EmotionMenkuratteiru.GetComponent<Animator>();
                Invoke("ChangeMenkuratteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("びっくりしている"))
            {
                Debug.Log("びっくりしている");
                animEmotionBikkurishiteiru = EmotionBikkurishiteiru.GetComponent<Animator>();
                Invoke("ChangeBikkurishiteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("途方に暮れている"))
            {
                Debug.Log("途方に暮れている");
                animEmotionTohounikureteiru = EmotionTohounikureteiru.GetComponent<Animator>();
                Invoke("ChangeTohounikureteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("悲しい"))
            {
                Debug.Log("悲しい");
                animEmotionKanashii = EmotionKanashii.GetComponent<Animator>();
                Invoke("ChangeKanashiiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("おびえている"))
            {
                Debug.Log("おびえている");
                animEmotionObieteiru = EmotionObieteiru.GetComponent<Animator>();
                Invoke("ChangeObieteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("心配している"))
            {
                Debug.Log("心配している");
                animEmotionShinpaishiteiru = EmotionShinpaishiteiru.GetComponent<Animator>();
                Invoke("ChangeShinpaishiteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("怖い"))
            {
                Debug.Log("怖い");
                animEmotionKowai = EmotionKowai.GetComponent<Animator>();
                Invoke("ChangeKowaiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("当惑している"))
            {
                Debug.Log("当惑している");
                animEmotionTouwakushiteiru = EmotionTouwakushiteiru.GetComponent<Animator>();
                Invoke("ChangeTouwakushiteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("落ち着かない"))
            {
                Debug.Log("落ち着かない");
                animEmotionOchitukanai = EmotionOchitukanai.GetComponent<Animator>();
                Invoke("ChangeOchitukanaiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("見限られている"))
            {
                Debug.Log("見限られている");
                animEmotionMikagirareteiru = EmotionMikagirareteiru.GetComponent<Animator>();
                Invoke("ChangeMikagirareteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("わびしい"))
            {
                Debug.Log("わびしい");
                animEmotionWabishii = EmotionWabishii.GetComponent<Animator>();
                Invoke("ChangeWabishiiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("落胆している"))
            {
                Debug.Log("落胆している");
                animEmotionRakutanshiteiru = EmotionRakutanshiteiru.GetComponent<Animator>();
                Invoke("ChangeRakutanshiteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("苦しい"))
            {
                Debug.Log("苦しい");
                animEmotionKurushii = EmotionKurushii.GetComponent<Animator>();
                Invoke("ChangeKurushiiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("やるせない"))
            {
                Debug.Log("やるせない");
                animEmotionYarusenai = EmotionYarusenai.GetComponent<Animator>();
                Invoke("ChangeYarusenaiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("つらい"))
            {
                Debug.Log("つらい");
                animEmotionTurai = EmotionTurai.GetComponent<Animator>();
                Invoke("ChangeTuraiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("悲痛な"))
            {
                Debug.Log("悲痛な");
                animEmotionHituuna = EmotionHituuna.GetComponent<Animator>();
                Invoke("ChangeHituunaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("孤独な"))
            {
                Debug.Log("孤独な");
                animEmotionKodokuna = EmotionKodokuna.GetComponent<Animator>();
                Invoke("ChangeKodokunaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("みじめな"))
            {
                Debug.Log("みじめな");
                animEmotionMijimena = EmotionMijimena.GetComponent<Animator>();
                Invoke("ChangeMijimenaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("打ちひしがれている"))
            {
                Debug.Log("打ちひしがれている");
                animEmotionUchihishigareteiru = EmotionUchihishigareteiru.GetComponent<Animator>();
                Invoke("ChangeUchihishigareteiruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("さびしい"))
            {
                Debug.Log("さびしい");
                animEmotionSabishii = EmotionSabishii.GetComponent<Animator>();
                Invoke("ChangeSabishiiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("むなしい"))
            {
                Debug.Log("むなしい");
                animEmotionMunashii = EmotionMunashii.GetComponent<Animator>();
                Invoke("ChangeMunashiiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("呆然"))
            {
                Debug.Log("呆然");
                animEmotionBouzen = EmotionBouzen.GetComponent<Animator>();
                Invoke("ChangeBouzenFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("放心"))
            {
                Debug.Log("放心");
                animEmotionHoushin = EmotionHoushin.GetComponent<Animator>();
                Invoke("ChangeHoushinFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("驚き"))
            {
                Debug.Log("驚き");
                animEmotionOdoroki = EmotionOdoroki.GetComponent<Animator>();
                Invoke("ChangeOdorokiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("仰天"))
            {
                Debug.Log("仰天");
                animEmotionGyuouten = EmotionGyuouten.GetComponent<Animator>();
                Invoke("ChangeGyuoutenFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("驚嘆"))
            {
                Debug.Log("驚嘆");
                animEmotionKyoutan = EmotionKyoutan.GetComponent<Animator>();
                Invoke("ChangeKyoutanFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("意外"))
            {
                Debug.Log("意外");
                animEmotionIgai = EmotionIgai.GetComponent<Animator>();
                Invoke("ChangeIgaiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("びっくりする"))
            {
                Debug.Log("びっくりする");
                animEmotionBikkurisuru = EmotionBikkurisuru.GetComponent<Animator>();
                Invoke("ChangeBikkurisuruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("衝撃"))
            {
                Debug.Log("衝撃");
                animEmotionShougeki = EmotionShougeki.GetComponent<Animator>();
                Invoke("ChangeShougekiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("当惑する"))
            {
                Debug.Log("当惑する");
                animEmotionTouwakusuru = EmotionTouwakusuru.GetComponent<Animator>();
                Invoke("ChangeTouwakusuruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("不信"))
            {
                Debug.Log("不信");
                animEmotionFushin = EmotionFushin.GetComponent<Animator>();
                Invoke("ChangeFushinFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("驚天動地"))
            {
                Debug.Log("驚天動地");
                animEmotionKyoutendouchi = EmotionKyoutendouchi.GetComponent<Animator>();
                Invoke("ChangeKyoutendouchiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("あっけにとられる"))
            {
                Debug.Log("あっけにとられる");
                animEmotionAkkenitorareru = EmotionAkkenitorareru.GetComponent<Animator>();
                Invoke("ChangeAkkenitorareruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("平穏な"))
            {
                Debug.Log("平穏な");
                animEmotionHeionna = EmotionHeionna.GetComponent<Animator>();
                Invoke("ChangeHeionnaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("幸せな喜び"))
            {
                Debug.Log("幸せな喜び");
                animEmotionSiawasenayorokobi = EmotionSiawasenayorokobi.GetComponent<Animator>();
                Invoke("ChangeSiawasenayorokobiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("有頂天な"))
            {
                Debug.Log("有頂天な ");
                animEmotionUchoutenna = EmotionUchoutenna.GetComponent<Animator>();
                Invoke("ChangeUchoutennaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("歓喜"))
            {
                Debug.Log("歓喜");
                animEmotionKanki = EmotionKanki.GetComponent<Animator>();
                Invoke("ChangeKankiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("落ち着く"))
            {
                Debug.Log("落ち着く");
                animEmotionOchituku = EmotionOchituku.GetComponent<Animator>();
                Invoke("ChangeOchitukuFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("幸福感"))
            {
                Debug.Log("幸福感");
                animEmotionKoufukukan = EmotionKoufukukan.GetComponent<Animator>();
                Invoke("ChangeKoufukukanFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("満足"))
            {
                Debug.Log("満足");
                animEmotionManzoku = EmotionManzoku.GetComponent<Animator>();
                Invoke("ChangeManzokuFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("嬉しい"))
            {
                Debug.Log("嬉しい");
                animEmotionUresii = EmotionUresii.GetComponent<Animator>();
                Invoke("ChangeUresiiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("大喜び"))
            {
                Debug.Log("大喜び");
                animEmotionOoyorokobi = EmotionOoyorokobi.GetComponent<Animator>();
                Invoke("ChangeOoyorokobiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("楽しそうな"))
            {
                Debug.Log("楽しそうな");
                animEmotionTanosisouna = EmotionTanosisouna.GetComponent<Animator>();
                Invoke("ChangeTanosisounaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("積極的な"))
            {
                Debug.Log("積極的な");
                animEmotionSekkyokutekina = EmotionSekkyokutekina.GetComponent<Animator>();
                Invoke("ChangeSekkyokutekinaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("退屈"))
            {
                Debug.Log("退屈");
                animEmotionTaikutuna = EmotionTaikutuna.GetComponent<Animator>();
                Invoke("ChangeTaikutunaFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("うんざり"))
            {
                Debug.Log("うんざり");
                animEmotionUnzari = EmotionUnzari.GetComponent<Animator>();
                Invoke("ChangeUnzariFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("強い嫌悪"))
            {
                Debug.Log("強い嫌悪");
                animEmotionTsuyoikeno = EmotionTsuyoikeno.GetComponent<Animator>();
                Invoke("ChangeTsuyoikenoFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("罵倒する"))
            {
                Debug.Log("罵倒する");
                animEmotionBatousuru = EmotionBatousuru.GetComponent<Animator>();
                Invoke("ChangeBatousuruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("ひどく嫌う"))
            {
                Debug.Log("ひどく嫌う");
                animEmotionHidokukirau = EmotionHidokukirau.GetComponent<Animator>();
                Invoke("ChangeHidokukirauFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("嫌気"))
            {
                Debug.Log("嫌気");
                animEmotionIyake = EmotionIyake.GetComponent<Animator>();
                Invoke("ChangeIyakeFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("道徳性のない"))
            {
                Debug.Log("道徳性のない");
                animEmotionDoutokuseinonai = EmotionDoutokuseinonai.GetComponent<Animator>();
                Invoke("ChangeDoutokuseinonaiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("ぞっとする"))
            {
                Debug.Log("ぞっとする");
                animEmotionZottosuru = EmotionZottosuru.GetComponent<Animator>();
                Invoke("ChangeZottosuruFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("うざい"))
            {
                Debug.Log("うざい");
                animEmotionUzai = EmotionUzai.GetComponent<Animator>();
                Invoke("ChangeUzaiFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("むかつく"))
            {
                Debug.Log("むかつく");
                animEmotionMukatsuku = EmotionMukatsuku.GetComponent<Animator>();
                Invoke("ChangeMukatsukuFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("憎悪"))
            {
                Debug.Log("憎悪");
                animEmotionZouo = EmotionZouo.GetComponent<Animator>();
                Invoke("ChangeZouoFlg", StopTime004 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions4[a]).Contains("復讐"))
            {
                Debug.Log("復讐");
                animEmotionFukushuu = EmotionFukushuu.GetComponent<Animator>();
                Invoke("ChangeFukushuuFlg", StopTime004 + (a * 5));
            }
        }

        Debug.Log("シーン５の開始したよ");

        Debug.Log(SaveEmotionsArray.emotions5.Count);
        for (int a = 0; a < SaveEmotionsArray.emotions5.Count; a++)
        {
            Debug.Log(SaveEmotionsArray.emotions5[a]);
        }

        for (int a = 0; a < SaveEmotionsArray.emotions5.Count; a++)
        {
            if ((SaveEmotionsArray.emotions5[a]).Contains("なげやりだ"))
            {
                Debug.Log("なげやりだ");
                animEmotionNageyarida = EmotionNageyarida.GetComponent<Animator>();
                Invoke("ChangeNageyaridaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("すねている"))
            {
                Debug.Log("すねている");
                animEmotionSuneteiru = EmotionSuneteiru.GetComponent<Animator>();
                Invoke("ChangeSuneteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("自暴自棄になっている"))
            {
                Debug.Log("自暴自棄になっている");
                animEmotionJiboujikininatteiru = EmotionJiboujikininatteiru.GetComponent<Animator>();
                Invoke("ChangeJiboujikininatteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("カッとしている"))
            {
                Debug.Log("カッとしている");
                animEmotionKattoshiteiru = EmotionKattoshiteiru.GetComponent<Animator>();
                Invoke("ChangeKattoshiteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("失望している"))
            {
                Debug.Log("失望している");
                animEmotionShitsuboushiteiru = EmotionShitsuboushiteiru.GetComponent<Animator>();
                Invoke("ChangeShitsuboushiteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("不愉快である"))
            {
                Debug.Log("不愉快である");
                animEmotionFuyukaidearu = EmotionFuyukaidearu.GetComponent<Animator>();
                Invoke("ChangeFuyukaidearuFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("怒っている"))
            {
                Debug.Log("怒っている");
                animEmotionOkotteiru = EmotionOkotteiru.GetComponent<Animator>();
                Invoke("ChangeOkotteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("キレる"))
            {
                Debug.Log("キレる");
                animEmotionKireru = EmotionKireru.GetComponent<Animator>();
                Invoke("ChangeKireruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("憎い"))
            {
                Debug.Log("憎い");
                animEmotionNikui = EmotionNikui.GetComponent<Animator>();
                Invoke("ChangeNikuiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("いらいらした"))
            {
                Debug.Log("いらいらした");
                animEmotionIrairashita = EmotionIrairashita.GetComponent<Animator>();
                Invoke("ChangeIrairashitaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("わずらわしい"))
            {
                Debug.Log("わずらわしい");
                animEmotionWazurawashii = EmotionWazurawashii.GetComponent<Animator>();
                Invoke("ChangeWazurawashiiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("腹立たしい"))
            {
                Debug.Log("腹立たしい");
                animEmotionHaratadashii = EmotionhHaratadashii.GetComponent<Animator>();
                Invoke("ChangeHaratadashiiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("狼狽している"))
            {
                Debug.Log("狼狽している");
                animEmotionRoubaishiteiru = EmotionRoubaishiteiru.GetComponent<Animator>();
                Invoke("ChangeRoubaishiteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("動揺している"))
            {
                Debug.Log("動揺している");
                animEmotionDouyoushiteiru = EmotionDouyoushiteiru.GetComponent<Animator>();
                Invoke("ChangeDouyoushiteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("うしろめたい"))
            {
                Debug.Log("うしろめたい");
                animEmotionUshirometai = EmotionUshirometai.GetComponent<Animator>();
                Invoke("ChangeUshirometaiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("面食らっている"))
            {
                Debug.Log("面食らっている");
                animEmotionMenkuratteiru = EmotionMenkuratteiru.GetComponent<Animator>();
                Invoke("ChangeMenkuratteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("びっくりしている"))
            {
                Debug.Log("びっくりしている");
                animEmotionBikkurishiteiru = EmotionBikkurishiteiru.GetComponent<Animator>();
                Invoke("ChangeBikkurishiteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("途方に暮れている"))
            {
                Debug.Log("途方に暮れている");
                animEmotionTohounikureteiru = EmotionTohounikureteiru.GetComponent<Animator>();
                Invoke("ChangeTohounikureteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("悲しい"))
            {
                Debug.Log("悲しい");
                animEmotionKanashii = EmotionKanashii.GetComponent<Animator>();
                Invoke("ChangeKanashiiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("おびえている"))
            {
                Debug.Log("おびえている");
                animEmotionObieteiru = EmotionObieteiru.GetComponent<Animator>();
                Invoke("ChangeObieteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("心配している"))
            {
                Debug.Log("心配している");
                animEmotionShinpaishiteiru = EmotionShinpaishiteiru.GetComponent<Animator>();
                Invoke("ChangeShinpaishiteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("怖い"))
            {
                Debug.Log("怖い");
                animEmotionKowai = EmotionKowai.GetComponent<Animator>();
                Invoke("ChangeKowaiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("当惑している"))
            {
                Debug.Log("当惑している");
                animEmotionTouwakushiteiru = EmotionTouwakushiteiru.GetComponent<Animator>();
                Invoke("ChangeTouwakushiteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("落ち着かない"))
            {
                Debug.Log("落ち着かない");
                animEmotionOchitukanai = EmotionOchitukanai.GetComponent<Animator>();
                Invoke("ChangeOchitukanaiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("見限られている"))
            {
                Debug.Log("見限られている");
                animEmotionMikagirareteiru = EmotionMikagirareteiru.GetComponent<Animator>();
                Invoke("ChangeMikagirareteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("わびしい"))
            {
                Debug.Log("わびしい");
                animEmotionWabishii = EmotionWabishii.GetComponent<Animator>();
                Invoke("ChangeWabishiiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("落胆している"))
            {
                Debug.Log("落胆している");
                animEmotionRakutanshiteiru = EmotionRakutanshiteiru.GetComponent<Animator>();
                Invoke("ChangeRakutanshiteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("苦しい"))
            {
                Debug.Log("苦しい");
                animEmotionKurushii = EmotionKurushii.GetComponent<Animator>();
                Invoke("ChangeKurushiiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("やるせない"))
            {
                Debug.Log("やるせない");
                animEmotionYarusenai = EmotionYarusenai.GetComponent<Animator>();
                Invoke("ChangeYarusenaiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("つらい"))
            {
                Debug.Log("つらい");
                animEmotionTurai = EmotionTurai.GetComponent<Animator>();
                Invoke("ChangeTuraiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("悲痛な"))
            {
                Debug.Log("悲痛な");
                animEmotionHituuna = EmotionHituuna.GetComponent<Animator>();
                Invoke("ChangeHituunaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("孤独な"))
            {
                Debug.Log("孤独な");
                animEmotionKodokuna = EmotionKodokuna.GetComponent<Animator>();
                Invoke("ChangeKodokunaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("みじめな"))
            {
                Debug.Log("みじめな");
                animEmotionMijimena = EmotionMijimena.GetComponent<Animator>();
                Invoke("ChangeMijimenaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("打ちひしがれている"))
            {
                Debug.Log("打ちひしがれている");
                animEmotionUchihishigareteiru = EmotionUchihishigareteiru.GetComponent<Animator>();
                Invoke("ChangeUchihishigareteiruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("さびしい"))
            {
                Debug.Log("さびしい");
                animEmotionSabishii = EmotionSabishii.GetComponent<Animator>();
                Invoke("ChangeSabishiiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("むなしい"))
            {
                Debug.Log("むなしい");
                animEmotionMunashii = EmotionMunashii.GetComponent<Animator>();
                Invoke("ChangeMunashiiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("呆然"))
            {
                Debug.Log("呆然");
                animEmotionBouzen = EmotionBouzen.GetComponent<Animator>();
                Invoke("ChangeBouzenFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("放心"))
            {
                Debug.Log("放心");
                animEmotionHoushin = EmotionHoushin.GetComponent<Animator>();
                Invoke("ChangeHoushinFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("驚き"))
            {
                Debug.Log("驚き");
                animEmotionOdoroki = EmotionOdoroki.GetComponent<Animator>();
                Invoke("ChangeOdorokiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("仰天"))
            {
                Debug.Log("仰天");
                animEmotionGyuouten = EmotionGyuouten.GetComponent<Animator>();
                Invoke("ChangeGyuoutenFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("驚嘆"))
            {
                Debug.Log("驚嘆");
                animEmotionKyoutan = EmotionKyoutan.GetComponent<Animator>();
                Invoke("ChangeKyoutanFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("意外"))
            {
                Debug.Log("意外");
                animEmotionIgai = EmotionIgai.GetComponent<Animator>();
                Invoke("ChangeIgaiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("びっくりする"))
            {
                Debug.Log("びっくりする");
                animEmotionBikkurisuru = EmotionBikkurisuru.GetComponent<Animator>();
                Invoke("ChangeBikkurisuruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("衝撃"))
            {
                Debug.Log("衝撃");
                animEmotionShougeki = EmotionShougeki.GetComponent<Animator>();
                Invoke("ChangeShougekiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("当惑する"))
            {
                Debug.Log("当惑する");
                animEmotionTouwakusuru = EmotionTouwakusuru.GetComponent<Animator>();
                Invoke("ChangeTouwakusuruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("不信"))
            {
                Debug.Log("不信");
                animEmotionFushin = EmotionFushin.GetComponent<Animator>();
                Invoke("ChangeFushinFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("驚天動地"))
            {
                Debug.Log("驚天動地");
                animEmotionKyoutendouchi = EmotionKyoutendouchi.GetComponent<Animator>();
                Invoke("ChangeKyoutendouchiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("あっけにとられる"))
            {
                Debug.Log("あっけにとられる");
                animEmotionAkkenitorareru = EmotionAkkenitorareru.GetComponent<Animator>();
                Invoke("ChangeAkkenitorareruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("平穏な"))
            {
                Debug.Log("平穏な");
                animEmotionHeionna = EmotionHeionna.GetComponent<Animator>();
                Invoke("ChangeHeionnaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("幸せな喜び"))
            {
                Debug.Log("幸せな喜び");
                animEmotionSiawasenayorokobi = EmotionSiawasenayorokobi.GetComponent<Animator>();
                Invoke("ChangeSiawasenayorokobiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("有頂天な"))
            {
                Debug.Log("有頂天な ");
                animEmotionUchoutenna = EmotionUchoutenna.GetComponent<Animator>();
                Invoke("ChangeUchoutennaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("歓喜"))
            {
                Debug.Log("歓喜");
                animEmotionKanki = EmotionKanki.GetComponent<Animator>();
                Invoke("ChangeKankiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("落ち着く"))
            {
                Debug.Log("落ち着く");
                animEmotionOchituku = EmotionOchituku.GetComponent<Animator>();
                Invoke("ChangeOchitukuFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("幸福感"))
            {
                Debug.Log("幸福感");
                animEmotionKoufukukan = EmotionKoufukukan.GetComponent<Animator>();
                Invoke("ChangeKoufukukanFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("満足"))
            {
                Debug.Log("満足");
                animEmotionManzoku = EmotionManzoku.GetComponent<Animator>();
                Invoke("ChangeManzokuFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("嬉しい"))
            {
                Debug.Log("嬉しい");
                animEmotionUresii = EmotionUresii.GetComponent<Animator>();
                Invoke("ChangeUresiiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("大喜び"))
            {
                Debug.Log("大喜び");
                animEmotionOoyorokobi = EmotionOoyorokobi.GetComponent<Animator>();
                Invoke("ChangeOoyorokobiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("楽しそうな"))
            {
                Debug.Log("楽しそうな");
                animEmotionTanosisouna = EmotionTanosisouna.GetComponent<Animator>();
                Invoke("ChangeTanosisounaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("積極的な"))
            {
                Debug.Log("積極的な");
                animEmotionSekkyokutekina = EmotionSekkyokutekina.GetComponent<Animator>();
                Invoke("ChangeSekkyokutekinaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("退屈"))
            {
                Debug.Log("退屈");
                animEmotionTaikutuna = EmotionTaikutuna.GetComponent<Animator>();
                Invoke("ChangeTaikutunaFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("うんざり"))
            {
                Debug.Log("うんざり");
                animEmotionUnzari = EmotionUnzari.GetComponent<Animator>();
                Invoke("ChangeUnzariFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("強い嫌悪"))
            {
                Debug.Log("強い嫌悪");
                animEmotionTsuyoikeno = EmotionTsuyoikeno.GetComponent<Animator>();
                Invoke("ChangeTsuyoikenoFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("罵倒する"))
            {
                Debug.Log("罵倒する");
                animEmotionBatousuru = EmotionBatousuru.GetComponent<Animator>();
                Invoke("ChangeBatousuruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("ひどく嫌う"))
            {
                Debug.Log("ひどく嫌う");
                animEmotionHidokukirau = EmotionHidokukirau.GetComponent<Animator>();
                Invoke("ChangeHidokukirauFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("嫌気"))
            {
                Debug.Log("嫌気");
                animEmotionIyake = EmotionIyake.GetComponent<Animator>();
                Invoke("ChangeIyakeFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("道徳性のない"))
            {
                Debug.Log("道徳性のない");
                animEmotionDoutokuseinonai = EmotionDoutokuseinonai.GetComponent<Animator>();
                Invoke("ChangeDoutokuseinonaiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("ぞっとする"))
            {
                Debug.Log("ぞっとする");
                animEmotionZottosuru = EmotionZottosuru.GetComponent<Animator>();
                Invoke("ChangeZottosuruFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("うざい"))
            {
                Debug.Log("うざい");
                animEmotionUzai = EmotionUzai.GetComponent<Animator>();
                Invoke("ChangeUzaiFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("むかつく"))
            {
                Debug.Log("むかつく");
                animEmotionMukatsuku = EmotionMukatsuku.GetComponent<Animator>();
                Invoke("ChangeMukatsukuFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("憎悪"))
            {
                Debug.Log("憎悪");
                animEmotionZouo = EmotionZouo.GetComponent<Animator>();
                Invoke("ChangeZouoFlg", StopTime005 + (a * 5));
            }
            if ((SaveEmotionsArray.emotions5[a]).Contains("復讐"))
            {
                Debug.Log("復讐");
                animEmotionFukushuu = EmotionFukushuu.GetComponent<Animator>();
                Invoke("ChangeFukushuuFlg", StopTime004 + (a * 5));
            }
        }

    }

    void PlayDoorBell()
    {
        audioSourceDoorBell.Play();
    }
    void ChangeNageyaridaFlg()
    {
        animEmotionNageyarida.SetTrigger("NageyaridaTrigger");
    }
    void ChangeSuneteiruFlg()
    {
        animEmotionSuneteiru.SetTrigger("SuneteiruTrigger");
    }
    void ChangeJiboujikininatteiruFlg()
    {
        animEmotionJiboujikininatteiru.SetTrigger("JiboujikininatteiruTrigger");
    }
    void ChangeKattoshiteiruFlg()
    {
        animEmotionKattoshiteiru.SetTrigger("KattoshiteiruTrigger");
    }
    void ChangeShitsuboushiteiruFlg()
    {
        animEmotionShitsuboushiteiru.SetTrigger("ShitsuboushiteiruTrigger");
    }
    void ChangeFuyukaidearuFlg()
    {
        animEmotionFuyukaidearu.SetTrigger("FuyukaidearuTrigger");
    }
    void ChangeOkotteiruFlg()
    {
        animEmotionOkotteiru.SetTrigger("OkotteiruTrigger");
    }
    void ChangeKireruFlg()
    {
        animEmotionKireru.SetTrigger("KireruTrigger");
    }
    void ChangeNikuiFlg()
    {
        animEmotionNikui.SetTrigger("NikuiTrigger");
    }
    void ChangeIrairashitaFlg()
    {
        animEmotionIrairashita.SetTrigger("IrairashitaTrigger");
    }
    void ChangeWazurawashiiFlg()
    {
        animEmotionWazurawashii.SetTrigger("WazurawashiiTrigger");
    }
    void ChangeHaratadashiiFlg()
    {
        animEmotionHaratadashii.SetTrigger("HaratadashiiTrigger");
    }
    void ChangeRoubaishiteiruFlg()
    {
        animEmotionRoubaishiteiru.SetTrigger("RoubaishiteiruTrigger");
    }
    void ChangeDouyoushiteiruFlg()
    {
        animEmotionDouyoushiteiru.SetTrigger("DouyoushiteiruTrigger");
    }
    void ChangeUshirometaiFlg()
    {
        animEmotionUshirometai.SetTrigger("UshirometaiTrigger");
    }
    void ChangeMenkuratteiruFlg()
    {
        animEmotionMenkuratteiru.SetTrigger("MenkuratteiruTrigger");
    }
    void ChangeBikkurishiteiruFlg()
    {
        animEmotionBikkurishiteiru.SetTrigger("BikkurishiteiruTrigger");
    }
    void ChangeTohounikureteiruFlg()
    {
        animEmotionTohounikureteiru.SetTrigger("TohounikureteiruTrigger");
    }
    void ChangeKanashiiFlg()
    {
        animEmotionKanashii.SetTrigger("KanashiiTrigger");
    }
    void ChangeObieteiruFlg()
    {
        animEmotionObieteiru.SetTrigger("ObieteiruTrigger");
    }
    void ChangeShinpaishiteiruFlg()
    {
        animEmotionShinpaishiteiru.SetTrigger("ShinpaishiteiruTrigger");
    }
    void ChangeKowaiFlg()
    {
        animEmotionKowai.SetTrigger("KowaiTrigger");
    }
    void ChangeTouwakushiteiruFlg()
    {
        animEmotionTouwakushiteiru.SetTrigger("TouwakushiteiruTrigger");
    }
    void ChangeOchitukanaiFlg()
    {
        animEmotionOchitukanai.SetTrigger("OchitukanaiTrigger");
    }
    void ChangeMikagirareteiruFlg()
    {
        animEmotionMikagirareteiru.SetTrigger("MikagirareteiruTrigger");
    }
    void ChangeWabishiiFlg()
    {
        animEmotionWabishii.SetTrigger("WabishiiTrigger");
    }
    void ChangeRakutanshiteiruFlg()
    {
        animEmotionRakutanshiteiru.SetTrigger("RakutanshiteiruTrigger");
    }
    void ChangeKurushiiFlg()
    {
        animEmotionKurushii.SetTrigger("KurushiiTrigger");
    }
    void ChangeYarusenaiFlg()
    {
        animEmotionYarusenai.SetTrigger("YarusenaiTrigger");
    }
    void ChangeTuraiFlg()
    {
        animEmotionTurai.SetTrigger("TuraiTrigger");
    }
    void ChangeHituunaFlg()
    {
        animEmotionHituuna.SetTrigger("HituunaTrigger");
    }
    void ChangeKodokunaFlg()
    {
        animEmotionKodokuna.SetTrigger("KodokunaTrigger");
    }
    void ChangeMijimenaFlg()
    {
        animEmotionMijimena.SetTrigger("MijimenaTrigger");
    }
    void ChangeUchihishigareteiruFlg()
    {
        animEmotionUchihishigareteiru.SetTrigger("UchihishigareteiruTrigger");
    }
    void ChangeSabishiiFlg()
    {
        animEmotionSabishii.SetTrigger("SabishiiTrigger");
    }
    void ChangeMunashiiFlg()
    {
        animEmotionMunashii.SetTrigger("MunashiiTrigger");
    }
    void ChangeBouzenFlg()
    {
        animEmotionBouzen.SetTrigger("BouzenTrigger");
    }
    void ChangeHoushinFlg()
    {
        animEmotionHoushin.SetTrigger("HoushinTrigger");
    }
    void ChangeOdorokiFlg()
    {
        animEmotionOdoroki.SetTrigger("OdorokiTrigger");
    }
    void ChangeGyuoutenFlg()
    {
        animEmotionGyuouten.SetTrigger("GyuoutenTrigger");
    }
    void ChangeKyoutanFlg()
    {
        animEmotionKyoutan.SetTrigger("KyoutanTrigger");
    }
    void ChangeIgaiFlg()
    {
        animEmotionIgai.SetTrigger("IgaiTrigger");
    }
    void ChangeBikkurisuruFlg()
    {
        animEmotionBikkurisuru.SetTrigger("BikkurisuruTrigger");
    }
    void ChangeShougekiFlg()
    {
        animEmotionShougeki.SetTrigger("ShougekiTrigger");
    }
    void ChangeTouwakusuruFlg()
    {
        animEmotionTouwakusuru.SetTrigger("TouwakusuruTrigger");
    }
    void ChangeFushinFlg()
    {
        animEmotionFushin.SetTrigger("FushinTrigger");
    }
    void ChangeKyoutendouchiFlg()
    {
        animEmotionKyoutendouchi.SetTrigger("KyoutendouchiTrigger");
    }
    void ChangeAkkenitorareruFlg()
    {
        animEmotionAkkenitorareru.SetTrigger("AkkenitorareruTrigger");
    }
    void ChangeHeionnaFlg()
    {
        animEmotionHeionna.SetTrigger("HeionnaTrigger");
    }
    void ChangeSiawasenayorokobiFlg()
    {
        animEmotionSiawasenayorokobi.SetTrigger("SiawasenayorokobiTrigger");
    }
    void ChangeUchoutennaFlg()
    {
        animEmotionUchoutenna.SetTrigger("UchoutennaTrigger");
    }
    void ChangeKankiFlg()
    {
        animEmotionKanki.SetTrigger("KankiTrigger");
    }
    void ChangeOchitukuFlg()
    {
        animEmotionOchituku.SetTrigger("OchitukuTrigger");
    }
    void ChangeKoufukukanFlg()
    {
        animEmotionKoufukukan.SetTrigger("KoufukukanTrigger");
    }
    void ChangeManzokuFlg()
    {
        animEmotionManzoku.SetTrigger("ManzokuTrigger");
    }
    void ChangeUresiiFlg()
    {
        animEmotionUresii.SetTrigger("UresiiTrigger");
    }
    void ChangeOoyorokobiFlg()
    {
        animEmotionOoyorokobi.SetTrigger("OoyorokobiTrigger");
    }
    void ChangeTanosisounaFlg()
    {
        animEmotionTanosisouna.SetTrigger("TanosisounaTrigger");
    }
    void ChangeSekkyokutekinaFlg()
    {
        animEmotionSekkyokutekina.SetTrigger("SekkyokutekinaTrigger");
    }
    void ChangeTaikutunaFlg()
    {
        animEmotionTaikutuna.SetTrigger("TaikutunaTrigger");
    }
    void ChangeUnzariFlg()
    {
        animEmotionUnzari.SetTrigger("UnzariTrigger");
    }
    void ChangeTsuyoikenoFlg()
    {
        animEmotionTsuyoikeno.SetTrigger("TsuyoikenoTrigger");
    }
    void ChangeBatousuruFlg()
    {
        animEmotionBatousuru.SetTrigger("BatousuruTrigger");
    }
    void ChangeHidokukirauFlg()
    {
        animEmotionHidokukirau.SetTrigger("HidokukirauTrigger");
    }
    void ChangeIyakeFlg()
    {
        animEmotionIyake.SetTrigger("IyakeTrigger");
    }
    void ChangeDoutokuseinonaiFlg()
    {
        animEmotionDoutokuseinonai.SetTrigger("DoutokuseinonaiTrigger");
    }
    void ChangeZottosuruFlg()
    {
        animEmotionZottosuru.SetTrigger("ZottosuruTrigger");
    }
    void ChangeUzaiFlg()
    {
        animEmotionUzai.SetTrigger("UzaiTrigger");
    }
    void ChangeMukatsukuFlg()
    {
        animEmotionMukatsuku.SetTrigger("MukatsukuTrigger");
    }
    void ChangeZouoFlg()
    {
        animEmotionZouo.SetTrigger("ZouoTrigger");
    }
    void ChangeFukushuuFlg()
    {
        animEmotionFukushuu.SetTrigger("FukushuuTrigger");
    }
    /*
    void ChangeIrairashitaFlg()
    {
        videoPlayer.Pause();
        animEmotionIrairashita.SetBool("irairashitaFlg", true);
        Invoke("MovieStart", 5f);
    }

    void ChangeYarusenaiFlg()
    {
        videoPlayer.Pause();
        animEmotionYarusenai.SetBool("yarusenaiFlg", true);
        Invoke("MovieStart", 3f);
    }

    void ChangeShinpaishiteiru2Flg()
    {
        videoPlayer.Pause();
        animEmotionShinpaishiteiru2.SetBool("shinpaishiteiru2Flg", true);
        Invoke("MovieStart", 3f);
    }
    void ChangeUnzariFlg()
    {
        videoPlayer.Pause();
        animEmotionUnzari.SetBool("unzariFlg", true);
        Invoke("MovieStart", 3f);
    }
    void ChangeFuyukaidaFlg()
    {
        videoPlayer.Pause();
        animEmotionFuyukaida.SetBool("fuyukaidaFlg", true);
        Invoke("MovieStart", 2f);
    }
    void ChangeRakutanshiteiruFlg()
    {
        videoPlayer.Pause();
        animEmotionRakutanshiteiru.SetBool("rakutanshiteiruFlg", true);
        Invoke("MovieStart", 6f);
    }
    void ChangeKurusiiFlg()
    {
        videoPlayer.Pause();
        animEmotionKurusii.SetBool("kurusiiFlg", true);
        Invoke("MovieStart", 5f);
    }
    */

    void MovieStart()
    {
        videoPlayer.Play();
    }

    void MoviePause()
    {
        videoPlayer.Pause();
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMovieFinished(VideoPlayer player)
    {

        player.Stop();
        SceneManager.LoadScene(NextMoveScene);
    }

}
